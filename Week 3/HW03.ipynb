{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 1: Post-Inverses, Pre-Inverses, and Projections\n",
    "\n",
    "For the first part of this assignment, we will review post (left) and pre (right) inverses and projections.\n",
    "\n",
    "For the purposes of this assignment, a projection is any function $p: X \\longrightarrow X$ such that $p \\circ p = p$.\n",
    "\n",
    "The function $f: X \\to Y$ is a pre-inverse (right inverse) for $g: Y \\to X$, or equivalently $g: Y \\to X$ is a post-inverse (left inverse) for $f: X \\to Y$, if and only if $g \\circ f = \\operatorname{Id}_X$, the identity function on the set $X$.\n",
    "\n",
    "However, another consequence of the fact that $g \\circ f = \\operatorname{Id}_X$ is the fact that $f \\circ g: Y \\to Y$ (first post-inverse then pre-inverse, the opposite of the usual order) is a _projection_, in other words $(f \\circ g) \\circ (f \\circ g) = f\\circ g$.\n",
    "\n",
    "This is because of the associativity of function composition. (Exercise: try to show that $g \\circ f = \\operatorname{Id}_X$ implies $(f \\circ g) \\circ (f \\circ g) = f\\circ g$ as claimed.)\n",
    "\n",
    "The function $f \\circ g : Y \\to Y$ is called a projection because every element $y$ of $Y$ is assigned to an element of $\\operatorname{Im}(f\\circ g)$, and for every element $y$ of $\\operatorname{Im}(f\\circ g)$ one has that $(f \\circ g)(y) = y$ or equivalently $(f \\circ g)_{|\\operatorname{Im}(f \\circ g)} = \\operatorname{Id}_{\\operatorname{Im}(f \\circ g)}$. \n",
    "\n",
    "In the cells below, we demonstrate some examples of this property using datasets you've looked at previously."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 State Capitals\n",
    "\n",
    "In the case where the function\n",
    "\n",
    "$$\\text{states} \\quad \\overset{f_1}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "is given by assigning to each state its capital, and the function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g}{\\longrightarrow} \\quad \\text{states} $$\n",
    "\n",
    "is given by assigning each city to the state in which it is located, as we showed earlier, $g \\circ f_1 = \\operatorname{Id}_{\\text{states}}$. Moreover, in this case the function \n",
    "\n",
    "$$\\text{cities} \\quad \\overset{f_1 \\circ g}{\\longrightarrow} \\quad \\text{cities}$$ \n",
    "\n",
    "sends every city to the capital of the state in which the city is located. In this case it should be intuitively clear that $(f_1 \\circ g) \\circ (f_1 \\circ g) = f_1 \\circ g$, since that just says that every state capital is located in the state of which it is the capital. We can also show that this is true using code.\n",
    "\n",
    "Let's review what the `states` and `western_cities` datasets look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import states\n",
    "\n",
    "states = states[[\"State\", \"Capital\"]]\n",
    "states"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import western_cities\n",
    "\n",
    "western_cities = western_cities[[\"City\", \"State\"]]\n",
    "western_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As explained earlier, the `states` dataset represents the function $f_1$ while the `western_cities` dataset represents the function $g$.\n",
    "\n",
    "Therefore $f_1 \\circ g$ is represented by left joining `western_cities` with `states` along the second column of `western_cities` and the first column of `states`. Modifying code from previous problems, do this in the cell below. Some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_copy1 = states.copy()\n",
    "states_copy1.columns = [\"State\", \"(f1 o g)(City)\"]\n",
    "western_cities_copy1 = western_cities.copy()\n",
    "f1_g = ...\n",
    "f1_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As claimed, this assignes every city to the capital of the state in which the city is located.\n",
    "\n",
    "In order to see what $(f_1 \\circ g) \\circ (f_1 \\circ g)$ looks like, we just need to join a copy of the `f1_g` table with itself. Do this in the cell below, modifying code from previous problems. Again some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f1_g_copy1 = f1_g.copy()\n",
    "f1_g_copy2 = f1_g.copy()\n",
    "f1_g_copy2.columns = [\"(f1 o g)(City)\", \"((f1 o g) o (f1 o g))(City)\"]\n",
    "f1_g_f1_g = ...\n",
    "f1_g_f1_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we claimed previously, $f_1 \\circ g$ and $(f_1 \\circ g) \\circ (f_1 \\circ g)$ are the same function, as can be verified by visually comparing the tables `f1_g` and `f1_g_f1_g`. For even more peace of mind we can use code to double-check this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f1_g[\"City\"].values == f1_g_f1_g[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f1_g[\"(f1 o g)(City)\"].values == f1_g_f1_g[\"((f1 o g) o (f1 o g))(City)\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we can also verify using code that the image of $f_1 \\circ g$ (and thus also of $(f_1 \\circ g) \\circ (f_1 \\circ g)$) is the set of all cities which are state capitals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f1_g = f1_g[\"(f1 o g)(City)\"].unique()\n",
    "image_f1_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to confirm using code that $f_1 \\circ g$ restricted to its image is the identity function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f1_g = f1_g[\"City\"].isin(image_f1_g)\n",
    "f1_g_restricted_to_image_f1_g = f1_g.loc[restriction_to_image_f1_g]\n",
    "f1_g_restricted_to_image_f1_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Most Populous Cities\n",
    "\n",
    "In the case where the function\n",
    "\n",
    "$$\\text{states} \\quad \\overset{f_2}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "is given by assigning to each state its most populous city, and the function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g}{\\longrightarrow} \\quad \\text{states} $$\n",
    "\n",
    "is given by assigning each city to the state in which it is located, as we showed earlier, $g \\circ f_2 = \\operatorname{Id}_{\\text{states}}$. Moreover, in this case the function \n",
    "\n",
    "$$\\text{cities} \\quad \\overset{f_2 \\circ g}{\\longrightarrow} \\quad \\text{cities}$$ \n",
    "\n",
    "sends every city to the most populous city of the state in which the city is located. In this case it should be intuitively clear that $(f_2 \\circ g) \\circ (f_2 \\circ g) = f_2 \\circ g$, since that just says that the most populous city of any state is located in the state of which it is the most populous city. We can also show that this is true using code.\n",
    "\n",
    "Let's review what the `most_populous_cities` and `western_cities` datasets look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import western_cities as western_cities_copy2\n",
    "\n",
    "western_cities_copy2 = western_cities_copy2.loc[\n",
    "    western_cities_copy2.groupby(\"State\")[\"Population (2010)\"].idxmax\n",
    "]\n",
    "most_populous_cities = western_cities_copy2[[\"State\", \"City\"]].reset_index(drop=True)\n",
    "most_populous_cities"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "western_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As explained earlier, the `most_populous_cities` dataset represents the function $f_2$ while the `western_cities` dataset represents the function $g$.\n",
    "\n",
    "Therefore $f_2 \\circ g$ is represented by left joining `western_cities` with `most_populous_cities` along the second column of `western_cities` and the first column of `most_populous_cities`. Modifying code from previous problems, do this in the cell below. Some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "western_cities_copy3 = western_cities.copy()\n",
    "most_populous_cities_copy1 = most_populous_cities.copy()\n",
    "most_populous_cities_copy1.columns = [\"State\", \"(f2 o g)(City)\"]\n",
    "f2_g = ...\n",
    "f2_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As promised, this assignes every city to the most populous city of the state in which the city is located.\n",
    "\n",
    "In order to see what $(f_2 \\circ g) \\circ (f_2 \\circ g)$ looks like, we just need to join a copy of the `f2_g` table with itself. Do this in the cell below, modifying code from previous problems. Again some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f2_g_copy1 = f2_g.copy()\n",
    "f2_g_copy2 = f2_g.copy()\n",
    "f2_g_copy2.columns = [\"(f2 o g)(City)\", \"((f2 o g) o (f2 o g))(City)\"]\n",
    "f2_g_f2_g = ...\n",
    "f2_g_f2_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be verified by visually comparing the tables `f2_g` and `f2_g_f2_g`, $f_2 \\circ g$ and $(f_2 \\circ g) \\circ (f_2 \\circ g)$ are the same function, as was claimed previously. For even more peace of mind we can use code to double-check this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f2_g[\"City\"].values == f2_g_f2_g[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f2_g[\"(f2 o g)(City)\"].values == f2_g_f2_g[\"((f2 o g) o (f2 o g))(City)\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we can also verify using code that the image of $f_2 \\circ g$ (and thus also of $(f_2 \\circ g) \\circ (f_2 \\circ g)$) is the set of all cities which are the most populous cities of some state. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f2_g = ...\n",
    "image_f2_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to confirm using code that $f_2 \\circ g$ restricted to its image is the identity function. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f2_g = ...\n",
    "f2_g_restricted_to_image_f2_g = ...\n",
    "f2_g_restricted_to_image_f2_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Circuit Courts\n",
    "\n",
    "In the case where the function\n",
    "\n",
    "$$\\text{metropolitan areas} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "is given by assigning to each metropolitan area its principal city, and the function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g_1}{\\longrightarrow} \\quad \\text{metropolitan areas} $$\n",
    "\n",
    "is given by assigning each city to the metropolitan area in which the courthouse of its Circuit Court is located, as we showed earlier, $g_1 \\circ f = \\operatorname{Id}_{\\text{metropolitan areas}}$. \n",
    "\n",
    "Moreover, in this case the function \n",
    "\n",
    "$$\\text{cities} \\quad \\overset{f \\circ g_1}{\\longrightarrow} \\quad \\text{cities}$$ \n",
    "\n",
    "sends every city to the principal city of the metropolitan area in which the courthouse of the city's Circuit Court is located. \n",
    "\n",
    "In this case the fact that $(f \\circ g_1) \\circ (f \\circ g_1) = f \\circ g_1$ says that the principal city of any metropolitan area is also the principal city of the metropolitan area in which the courthouse of its Circuit Court is located. While that isn't true for every metropolitan area, it is true for the subset of metropolitan areas found in the dataset `metropolises`, which consists only of metropolitan areas which contain the courthouse of a Circuit Court. \n",
    "\n",
    "We can also show that this is true for these special metropolitan areas using code.\n",
    "\n",
    "Let's review what the `metropolises` and `circuit_courts` datasets look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import metropolises\n",
    "\n",
    "metropolises = metropolises[[\"Metropolitan Statistical Area\", \"Principal City\"]]\n",
    "metropolises"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import circuit_courts\n",
    "\n",
    "circuit_courts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As explained earlier, the `metropolises` dataset represents the function $f$ while the `circuit_courts` dataset represents the function $g_1$.\n",
    "\n",
    "Therefore $f \\circ g_1$ is represented by left joining `circuit_courts` with `metropolises` along the second column of `circuit_courts` and the first column of `metropolises`. Modifying code from previous problems, do this in the cell below. Some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit_courts_copy1 = circuit_courts.copy()\n",
    "circuit_courts_copy1.columns = [\"City\", \"Metropolitan Area\"]\n",
    "metropolises_copy1 = metropolises.copy()\n",
    "metropolises_copy1.columns = [\"Metropolitan Area\", \"(f o g1)(City)\"]\n",
    "f_g1 = ...\n",
    "f_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As promised, this assigns every city to the principal city of the metropolitan area in which the courthouse of its Circuit Court is located.\n",
    "\n",
    "In order to see what $(f \\circ g_1) \\circ (f \\circ g_1)$ looks like, we just need to join a copy of the `f_g1` table with itself. Do this in the cell below, modifying code from previous problems. Again some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g1_copy1 = f_g1.copy()\n",
    "f_g1_copy2 = f_g1.copy()\n",
    "f_g1_copy2.columns = [\"(f o g1)(City)\", \"((f o g1) o (f o g1))(City)\"]\n",
    "f_g1_f_g1 = ...\n",
    "f_g1_f_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be verified by visually comparing the tables `f_g1` and `f_g1_f_g1`, the functions $f \\circ g_1$ and $(f \\circ g_1) \\circ (f \\circ g_1)$ are the same, as was claimed previously. For even more peace of mind we can use code to double-check this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g1[\"City\"].values == f_g1[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g1[\"(f o g1)(City)\"].values == f_g1_f_g1[\"((f o g1) o (f o g1))(City)\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we can also verify using code that the image of $f \\circ g_1$ (and thus also of $(f \\circ g_1) \\circ (f \\circ g_1)$) is the set of all cities which are the principal city of some metropolitan area in which the courthouse of a Circuit Court is located. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f_g1 = ...\n",
    "image_f_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to confirm using code that $f \\circ g_1$ restricted to its image is the identity function. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f_g1 = ...\n",
    "f_g1_restricted_to_image_f_g1 = ...\n",
    "f_g1_restricted_to_image_f_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.4 Federal Reserve Districts\n",
    "\n",
    "In the case where the function\n",
    "\n",
    "$$\\text{metropolitan areas} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "is given by assigning to each metropolitan area its principal city, and the function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g_2}{\\longrightarrow} \\quad \\text{metropolitan areas} $$\n",
    "\n",
    "is given by assigning each city to the metropolitan area in which the headquarters of its Federal Reserve District is located, as we showed earlier, $g_2 \\circ f = \\operatorname{Id}_{\\text{metropolitan areas}}$. \n",
    "\n",
    "Moreover, in this case the function \n",
    "\n",
    "$$\\text{cities} \\quad \\overset{f \\circ g_2}{\\longrightarrow} \\quad \\text{cities}$$ \n",
    "\n",
    "sends every city to the principal city of the metropolitan area in which the headquarters of the city's Federal Reserve District is located. \n",
    "\n",
    "In this case the fact that $(f \\circ g_2) \\circ (f \\circ g_2) = f \\circ g_2$ says that the principal city of any metropolitan area is also the principal city of the metropolitan area in which the headquarters of its Federal Reserve District is located. While that isn't true for every metropolitan area, it is true for the subset of metropolitan areas found in the dataset `metropolises`, which consists only of metropolitan areas which contain the headquarters of a Federal Reserve District. \n",
    "\n",
    "We can also show that this is true for these special metropolitan areas using code.\n",
    "\n",
    "Let's review what the `metropolises` and `federal_reserve_districts` datasets look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metropolises"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import federal_reserve_districts\n",
    "\n",
    "federal_reserve_districts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As explained earlier, the `metropolises` dataset represents the function $f$ while the `federal_reserve_districts` dataset represents the function $g_2$.\n",
    "\n",
    "Therefore $f \\circ g_2$ is represented by left joining `federal_reserve_districts` with `metropolises` along the second column of `federal_reserve_districts` and the first column of `metropolises`. Modifying code from previous problems, do this in the cell below. Some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "federal_reserve_districts_copy1 = federal_reserve_districts.copy()\n",
    "federal_reserve_districts_copy1.columns = [\"City\", \"Metropolitan Area\"]\n",
    "metropolises_copy2 = metropolises.copy()\n",
    "metropolises_copy2.columns = [\"Metropolitan Area\", \"(f o g2)(City)\"]\n",
    "f_g2 = ...\n",
    "f_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As promised, this assigns every city to the principal city of the metropolitan area in which the headquarters of its Federal Reserve District is located.\n",
    "\n",
    "In order to see what $(f \\circ g_2) \\circ (f \\circ g_2)$ looks like, we just need to join a copy of the `f_g2` table with itself. Do this in the cell below, modifying code from previous problems. Again some of the code is given for you already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g2_copy1 = f_g2.copy()\n",
    "f_g2_copy2 = f_g2.copy()\n",
    "f_g2_copy2.columns = [\"(f o g2)(City)\", \"((f o g2) o (f o g2))(City)\"]\n",
    "f_g2_f_g2 = ...\n",
    "f_g2_f_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be verified by visually comparing the tables `f_g2` and `f_g2_f_g2`, the functions $f \\circ g_2$ and $(f \\circ g_2) \\circ (f \\circ g_2)$ are the same, as was claimed previously. For even more peace of mind we can use code to double-check this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g2[\"City\"].values == f_g2[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f_g2[\"(f o g2)(City)\"].values == f_g2_f_g2[\"((f o g2) o (f o g2))(City)\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Moreover, we can also verify using code that the image of $f \\circ g_2$ (and thus also of $(f \\circ g_2) \\circ (f \\circ g_2)$) is the set of all cities which are the principal city of some metropolitan area in which the headquarters of a Federal Reserve District is located. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f_g2 = ...\n",
    "image_f_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to confirm using code that $f \\circ g_2$ restricted to its image is the identity function. Modify code from a previous problem to do this in the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f_g2 = ...\n",
    "f_g2_restricted_to_image_f_g2 = ...\n",
    "f_g2_restricted_to_image_f_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Part 2: Introduction to Norms\n",
    "\n",
    "## 2.1 Defining Functions to Compute Norms\n",
    "\n",
    "First, let's define a couple functions that will help us compute norms. You should know how to calculate $L_1$, $L_2$, and $L_\\infty$ norms by hand. Using what you learned about Python lists and loops in last Friday's lab, fill in the bodies of the following 3 functions.\n",
    "\n",
    "For now, we will pass in our vector as a Python list of $n$ numbers, for any arbitrary $n$. We will not be using numpy for this problem.\n",
    "\n",
    "When you run each cell, there will be a check to verify that your solution is correct. Make sure your solution passes all tests to receive credit for this question."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't change this cell; just run it.\n",
    "import numpy as np\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "plt.style.use(\"fivethirtyeight\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Congratulations! Your L1_norm function is correct!\n"
     ]
    }
   ],
   "source": [
    "def L1_norm(vector):\n",
    "    ...\n",
    "\n",
    "\n",
    "######################################\n",
    "## DO NOT MODIFY BELOW THIS COMMENT ##\n",
    "######################################\n",
    "\n",
    "## The remainder of this cell will verify the correctness of your L1_norm function\n",
    "num_iters, min_n, max_n, sigma = 1000, 10, 100, 10\n",
    "for _ in range(num_iters):\n",
    "    n = np.random.randint(min_n, max_n)\n",
    "    vec = sigma * np.random.randn(n)\n",
    "    lst = vec.tolist()\n",
    "    norm_np = np.linalg.norm(vec, ord=1)\n",
    "    norm_test = L1_norm(lst)\n",
    "    assert math.isclose(\n",
    "        norm_np, norm_test\n",
    "    ), \"Calculated value ({}) does not match the expected value ({}) for input {}\".format(\n",
    "        norm_test, norm_np, vec\n",
    "    )\n",
    "print(\"Congratulations! Your L1_norm function is correct!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Congratulations! Your L2_norm function is correct!\n"
     ]
    }
   ],
   "source": [
    "def L2_norm(vector):\n",
    "    ...\n",
    "\n",
    "\n",
    "######################################\n",
    "## DO NOT MODIFY BELOW THIS COMMENT ##\n",
    "######################################\n",
    "\n",
    "## The remainder of this cell will verify the correctness of your L2_norm function\n",
    "num_iters, min_n, max_n, sigma = 1000, 10, 100, 10\n",
    "for _ in range(num_iters):\n",
    "    n = np.random.randint(min_n, max_n)\n",
    "    vec = sigma * np.random.randn(n)\n",
    "    lst = vec.tolist()\n",
    "    norm_np = np.linalg.norm(vec, ord=2)\n",
    "    norm_test = L2_norm(lst)\n",
    "    assert math.isclose(\n",
    "        norm_np, norm_test\n",
    "    ), \"Calculated value ({}) does not match the expected value ({}) for input {}\".format(\n",
    "        norm_test, norm_np, vec\n",
    "    )\n",
    "print(\"Congratulations! Your L2_norm function is correct!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Congratulations! Your Linf_norm function is correct!\n"
     ]
    }
   ],
   "source": [
    "def Linf_norm(vector):\n",
    "    ...\n",
    "\n",
    "\n",
    "######################################\n",
    "## DO NOT MODIFY BELOW THIS COMMENT ##\n",
    "######################################\n",
    "\n",
    "## The remainder of this cell will verify the correctness of your Linf_norm function\n",
    "num_iters, min_n, max_n, sigma = 1000, 10, 100, 10\n",
    "for _ in range(num_iters):\n",
    "    n = np.random.randint(min_n, max_n)\n",
    "    vec = sigma * np.random.randn(n)\n",
    "    lst = vec.tolist()\n",
    "    norm_np = np.linalg.norm(vec, ord=np.inf)\n",
    "    norm_test = Linf_norm(lst)\n",
    "    assert math.isclose(\n",
    "        norm_np, norm_test\n",
    "    ), \"Calculated value ({}) does not match the expected value ({}) for input {}\".format(\n",
    "        norm_test, norm_np, vec\n",
    "    )\n",
    "print(\"Congratulations! Your Linf_norm function is correct!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 A Generalized Function to Compute Norms\n",
    "\n",
    "As mentioned in the notes, there are many other norms than $L_1$, $L_2$, $L_\\infty$. One family of norms is known as the [$L_p$ norm](https://en.wikipedia.org/wiki/Norm_(mathematics)#p-norm) (which, the $L_1$ and $L_2$ norms belong to). The $L_p$ norm is defined as such:\n",
    "\n",
    "$\\|x\\|_p = \\Big(\\sum_{i=1}^{n}|x_i|^p\\Big)^{1/p}$\n",
    "\n",
    "Notice that the $L_1$ and $L_2$ norms are simply special cases, where $p=1$ or $p=2$. As such, let's try to write a generalized norm calculation function that is able to calculate any $L_p$ norm, given a vector and a $p$.\n",
    "\n",
    "Again, let's not use numpy for now. There will also be a correctness check at the bottom of the cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Congratulations! Your Lp_norm function is correct!\n"
     ]
    }
   ],
   "source": [
    "def Lp_norm(vector, p):\n",
    "    ...\n",
    "\n",
    "\n",
    "######################################\n",
    "## DO NOT MODIFY BELOW THIS COMMENT ##\n",
    "######################################\n",
    "\n",
    "## The remainder of this cell will verify the correctness of your Lp_norm function\n",
    "num_iters, min_n, max_n, sigma = 1000, 10, 100, 10\n",
    "min_p, max_p = 1, 100\n",
    "for _ in range(num_iters):\n",
    "    p = (max_p - min_p) * np.random.rand() + min_p\n",
    "    n = np.random.randint(min_n, max_n)\n",
    "    vec = sigma * np.random.randn(n)\n",
    "    lst = vec.tolist()\n",
    "    norm_np = np.linalg.norm(vec, ord=p)\n",
    "    norm_test = Lp_norm(lst, p)\n",
    "    assert math.isclose(\n",
    "        norm_np, norm_test\n",
    "    ), \"Calculated value ({}) does not match the expected value ({}) for input {}\".format(\n",
    "        norm_test, norm_np, vec\n",
    "    )\n",
    "print(\"Congratulations! Your Lp_norm function is correct!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Demonstration (no question to answer): Why is the $L_\\infty$ norm called as such?\n",
    "\n",
    "As mentioned in the previous cell, the $L_\\infty$ norm is technically not a part of the $L_p$ norm family. However, it can be seen as taking the limit as $p\\to\\infty$. We can demonstrate this using the `Lp_norm` and `Linf_norm` functions we just wrote."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Our list: [12.983428885505877, -2.5884193570194407, 7.1575722307256076, 4.701649574580035, 18.464312949518575, -14.123625896055497, 3.5383126771842504, 12.526832443056522, 6.24509626738582, -10.34814409673805]\n",
      "\n",
      "The L_inf norm is: 18.464312949518575\n",
      "\n",
      "The L_1 norm is: 92.67739437776967\n",
      "The L_2 norm is: 33.23461827366429\n",
      "The L_3 norm is: 24.744424453223886\n",
      "The L_4 norm is: 21.788281947474424\n",
      "The L_5 norm is: 20.402801030994812\n",
      "The L_6 norm is: 19.654931705149483\n",
      "The L_7 norm is: 19.218890460494713\n",
      "The L_8 norm is: 18.952720549867085\n",
      "The L_9 norm is: 18.785374869480066\n",
      "The L_10 norm is: 18.677975426484913\n",
      "The L_20 norm is: 18.469846688624067\n",
      "The L_30 norm is: 18.464532664917733\n",
      "The L_40 norm is: 18.464323590068794\n",
      "The L_50 norm is: 18.46431351900644\n",
      "The L_60 norm is: 18.464312981733777\n",
      "The L_70 norm is: 18.46431295140415\n",
      "The L_80 norm is: 18.464312949631516\n",
      "The L_90 norm is: 18.464312949525457\n",
      "The L_100 norm is: 18.464312949519\n"
     ]
    }
   ],
   "source": [
    "## Let's randomly initiate a 10 dimensional vector\n",
    "sigma = 10\n",
    "vec = sigma * np.random.randn(10)\n",
    "lst = vec.tolist()\n",
    "\n",
    "print(\"Our list: {}\".format(lst))\n",
    "print()\n",
    "print(\"The L_inf norm is: {}\".format(Linf_norm(lst)))\n",
    "print()\n",
    "\n",
    "for p in range(1, 101):\n",
    "    if p > 10:\n",
    "        if p % 10 != 0:\n",
    "            continue\n",
    "    norm = Lp_norm(lst, p)\n",
    "    print(\"The L_{} norm is: {}\".format(p, norm))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how as $p$ increases, the $L_p$ norm approaches the $L_\\infty$ norm! Given what you know about exponents, why do you think this is the case?\n",
    "\n",
    "(Hint: look back at how the $L_p$ norm is defined)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.3 Defining Your Own Vectors\n",
    "\n",
    "Let's construct a simple vector to compute the norm of. This question corresponds with question 4 of the pencil and paper homework, in which you will be calculating the $L_1$, $L_2$, and $L_\\infty$ norms of the vector where $x_i = 1/i$.\n",
    "\n",
    "In the following function, given a length $n$ for the vector, construct the \"decay\" vector where $x_i = 1/i$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The decay vector of length 10 is:\n",
      "[1.0, 0.5, 0.3333333333333333, 0.25, 0.2, 0.16666666666666666, 0.14285714285714285, 0.125, 0.1111111111111111, 0.1]\n"
     ]
    }
   ],
   "source": [
    "def construct_decay(n):\n",
    "    ...\n",
    "\n",
    "\n",
    "######################################\n",
    "## DO NOT MODIFY BELOW THIS COMMENT ##\n",
    "######################################\n",
    "\n",
    "## No automated check here, but take a look at the output vector yourself\n",
    "## to make sure it looks correct.\n",
    "\n",
    "decay_10 = construct_decay(10)\n",
    "print(\"The decay vector of length 10 is:\\n{}\".format(decay_10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you know how to write a function that computes norms, we can switch over to using the pre-defined numpy methods to calculate norms. While your functions work well for most cases, the numpy ones are more robust and more convenient to use with vectors defined as numpy ndarrays (most of the vectors you will use throughout this course will be ndarrays).\n",
    "\n",
    "But first, let's learn how to convert a vector in Python list form into a vector defined as a ndarray."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "decay_10 is of type <class 'list'>\n"
     ]
    }
   ],
   "source": [
    "print(\"decay_10 is of type\", type(decay_10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert a `list` to an `np.array`, simply call `np.array()` on the `list`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "decay_vec = np.array(decay_10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again a sanity check to make sure the type is what we expect it to be:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "decay_vec is of type <class 'numpy.ndarray'>\n"
     ]
    }
   ],
   "source": [
    "print(\"decay_vec is of type\", type(decay_vec))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you ever need to change the `np.array` back to a `list`, you can use the `.tolist()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "decay_lst is of type <class 'list'>\n"
     ]
    }
   ],
   "source": [
    "decay_lst = decay_vec.tolist()\n",
    "\n",
    "## Sanity type check\n",
    "print(\"decay_lst is of type\", type(decay_lst))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's stick with the np.array version for now. To calculate the norm of an `np.array`, we can use the `np.linalg.norm()` function. Run the following cell to take a look at the documentation for `np.linalg.norm`. ([Typing a question mark is a useful trick which works generally to access the documentation of a function quickly and easily.](https://jakevdp.github.io/PythonDataScienceHandbook/01.01-help-and-documentation.html#Accessing-Documentation-with-?))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.linalg.norm?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that you are (hopefully) familiar with `np.linalg.norm`, let's use it to calculate the $L_1$, $L_2$, and $L_\\infty$ norms of the decay vector we just defined. If you are still slightly confused about how to use `np.linalg.norm`, you can also look above at the automated checks for the norm functions. We used `np.linalg.norm` to verify the correctness of the functions you wrote!\n",
    "\n",
    "In the cell below, fill in the blanks with calls to `np.linalg.norm` to calculate the norms of the decay vector. Do not use the functions you wrote above. Also, make sure you are calculating norms for the np.array representation of the decay vector, `decay_vec`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "decay_L1 = ...\n",
    "decay_L2 = ...\n",
    "decay_Linf = ...\n",
    "\n",
    "print(\"The L_1 norm of the decay vector is {}\".format(decay_L1))\n",
    "print(\"The L_2 norm of the decay vector is {}\".format(decay_L2))\n",
    "print(\"The L_inf norm of the decay vector is {}\".format(decay_Linf))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
