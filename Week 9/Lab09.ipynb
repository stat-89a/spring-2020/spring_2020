{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "is_imports_cell": true
   },
   "outputs": [],
   "source": [
    "# Don't change this cell; just run it.\n",
    "import numpy as np\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "%matplotlib inline\n",
    "plt.style.use(\"fivethirtyeight\")\n",
    "\n",
    "from linalg_for_datasci.plotting import (\n",
    "    plot_region_with_func,\n",
    "    plot_region_with_two_basises,\n",
    "    plot_3D_spectral_demo,\n",
    ")\n",
    "from linalg_for_datasci.linear_func import (\n",
    "    get_parts_for_diagonalization_demo,\n",
    "    apply_func_to_basis,\n",
    "    Rx, Ry, Rz,\n",
    ")\n",
    "\n",
    "from linalg_for_datasci.arrays import stringify_matrix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "is_title_cell": true
   },
   "source": [
    "## Symmetric Matrices in 2D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Review: Plotting Regions with Linear Functions\n",
    "\n",
    "Recall from Lab 04 where we plotted visualizations of the $\\mathbb{R}^2$ space after being changed by multiplication with a matrix $A$. In this problem, we are once again going to be looking at matrices as linear functions. Here is a brief review to refresh your memory on what we did back in Lab 04."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Identity\n",
    "A = np.eye(2)\n",
    "\n",
    "plot_region_with_func(A, title=\"Vectors before being modified by linear function\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Skew\n",
    "A = np.array([[1, 1], [0, 1]])\n",
    "\n",
    "plot_region_with_func(A)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Stretch along x-axis\n",
    "A = np.array([[3, 0], [0, 1]])\n",
    "\n",
    "plot_region_with_func(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Spectral Decomposition\n",
    "\n",
    "In class you learned that a symmetric matrix can be orthogonally diagonalized into $V\\Lambda V^T$, where $V$ is a unitary (rotation) matrix. Here, we will \"build\" a matrix step by step, by decomposing it into $V$, $\\Lambda$, and $V^T$, and seeing how each of those affect the $\\mathbb{R}^2$ space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's consider the matrix\n",
    "$$A = \\begin{bmatrix}2.5&\\frac{\\sqrt{3}}{2}\\\\\\frac{\\sqrt{3}}{2}&1.5\\end{bmatrix}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.array([[2.5, np.sqrt(3) / 2], [np.sqrt(3) / 2, 1.5]])\n",
    "\n",
    "\n",
    "d, V, v1, v2, e1, e2, angle = get_parts_for_diagonalization_demo(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start with the $\\mathbb{R}^2$ space with elementary basis vecotrs $e_1$ and $e_2$ overlaid before applying any linear function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T1 = np.eye(2)\n",
    "\n",
    "\n",
    "plot_region_with_func(T1, title=\"Vectors before being modified by linear function\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's see where the eigenvectors come into play. In this second plot, we will overlay the eigenbasis vectors in magenta. Notice that they are orthogonal with each other, but not aligned with the coordinate axis like the $e_1$ and $e_2$ basis vectors are."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T2 = np.eye(2)\n",
    "\n",
    "eigenbasis = [v1, v2]\n",
    "standard_basis = [e1, e2]\n",
    "\n",
    "plot_region_with_two_basises(\n",
    "    T2,\n",
    "    \"Vectors before being modified by linear function, eigenbasis overlaid\",\n",
    "    eigenbasis,\n",
    "    standard_basis,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are used to having the $e_1$ and $e_2$ elementary basis vectors being aligned with the coordinate axes. But what if we performed a change of basis, so that the eigenbasis vectors, $v_1$ and $v_2$, are aligned with the coordinate axes? We can achieve this by rotating everything clockwise by 30 degrees. Notice now that the magenta $v_1$ and $v_2$ vectors appear to be aligned to the coordinate axes. What's special about this basis? We will see in the next two plots."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T3 = V.T\n",
    "\n",
    "eigenbasis = apply_func_to_basis(T3, [v1, v2])\n",
    "standard_basis = apply_func_to_basis(T3, [e1, e2])\n",
    "\n",
    "plot_region_with_two_basises(\n",
    "    T3,\n",
    "    \"Vectors after being modified by $1$st linear function\",\n",
    "    eigenbasis,\n",
    "    standard_basis,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the fourth plot, we apply $\\Lambda$, a diagonal matrix. No rotational trickery here, $\\Lambda$ simply stretches things out along the coordinate axes. In this case, we stretch by a factor of $3$ along the $x-$axis and a factor of $1$ along the $y-$axis. Notice how the eigenvectors $v_1$ and $v_2$ are still orthogonal to each other and aligned to the $x-$ and $y-$axes, but the elementary basis vectors $e_1$ and $e_2$ are no longer orthogonal to each other."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T4 = np.diag(d) @ V.T\n",
    "\n",
    "eigenbasis = apply_func_to_basis(T4, [v1, v2])\n",
    "standard_basis = apply_func_to_basis(T4, [e1, e2])\n",
    "\n",
    "plot_region_with_two_basises(\n",
    "    T4,\n",
    "    \"Vectors after being modified by $2$nd linear function\",\n",
    "    eigenbasis,\n",
    "    standard_basis,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the fifth plot, we apply $V$, which rotates everything CCW again by $30$ degrees. Notice how $v_1$ and $v_2$ are no longer aligned with the $x-$ and $y-$axes, but neither are $e_1$ and $e_2$ even though we rotated back! This is because we have now applied the entire $A$ matrix on the $\\mathbb{R}^2$ space!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T5 = V @ np.diag(d) @ V.T\n",
    "\n",
    "eigenbasis = apply_func_to_basis(T5, [v1, v2])\n",
    "standard_basis = apply_func_to_basis(T5, [e1, e2])\n",
    "\n",
    "plot_region_with_two_basises(\n",
    "    T5,\n",
    "    \"Vectors after being modified by $3$rd linear function\",\n",
    "    eigenbasis,\n",
    "    standard_basis,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How did we do it such that the \"direction\" of $v_1$ and $v_2$ are preserved? Notice that since $\\Lambda$ is always \"sandwiched\" between $U$ and $U^T$, we are only ever doing any stretching when the eigenvectors are aligned with the coordinate axes. Since they are aligned, stretching along the coordinate axes will preserve their orthogonality! Thus, since unitary operations (in this case, rotation) preserve angles between vectors, once we rotate back, their \"directions\" are still preserved! Does this agree with what you know about eigenvectors and how they behave under linear functions?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lastly, let's compare this to directly applying the $A$ matrix as a function to the $\\mathbb{R}^2$ space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.array([[2.5, np.sqrt(3) / 2], [np.sqrt(3) / 2, 1.5]])\n",
    "\n",
    "plot_region_with_func(A, \"Vectors after being modified by $3$rd linear function\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the rectangular region looks identical!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "is_title_cell": true
   },
   "source": [
    "## Symmetric Matrices in 3D"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. 3D Spectral Decomposition Demo\n",
    "\n",
    "Let's consider a $3\\times3$ symmetric matrix. Just as how we visualized a $2\\times2$ symmetric matrix as a morphed rectangle, we can visualize a $3\\times3$ symmetric matrix as a morphed rectangular prism. The only difference here is that instead of a simple 2D rotation matrix, our orthogonal function is a much more complex 3D orthogonal function, comprised of $3$ separate rotation angles. However, now that we are familiar with the 2D case, we can black box the 3D orthogonal function simply as something that reorients the space so that the eigenvectors are aligned with the $x-$, $y-$, and $z-$axes, while lengths and preserving angles between vectors. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Consider the following $A$ matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "V = Rz(np.pi / 6) @ Ry(np.pi / 6) @ Rx(np.pi / 6)\n",
    "s = np.array([3, 1, 1])\n",
    "S = np.diag(s)\n",
    "A = V @ S @ V.T\n",
    "\n",
    "print(\"A:\\n{}\".format(stringify_matrix(A)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same $4$ main steps apply in the 3D case. Keep in mind that it is still $A = V\\Lambda V^T$.\n",
    "\n",
    "In the first plot, we have the $\\mathbb{R}^3$ space with $e_1, e_2, e_3$ aligned with the $x-$, $y-$, and $z-$axes. This means that relative to the orientation where eigenvectors $v_1, v_2, v_3$ are algined with the axes, all vectors in the space is are modified by multiplying by $V$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T1 = np.eye(3)\n",
    "\n",
    "plot_3D_spectral_demo(transformation_matrix=T1, eigenbasis_matrix=V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the second plot, we apply $V^T$. This performs a change of basis into the eigenbasis, where the eigenvectors are aligned with the axes. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T2 = V.T\n",
    "\n",
    "plot_3D_spectral_demo(transformation_matrix=T2, eigenbasis_matrix=V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the third plot, we apply $S$ and stretch along the $x-$, $y-$, and $z-$axes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T3 = S @ V.T\n",
    "\n",
    "plot_3D_spectral_demo(transformation_matrix=T3, eigenbasis_matrix=V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the fourth plot, we go back to the elementary basis by applying the change of basis $V$ (we can view the orthogonal function as a change of basis)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T4 = V @ S @ V.T\n",
    "\n",
    "plot_3D_spectral_demo(transformation_matrix=T4, eigenbasis_matrix=V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare this to directly applying the function $A$ onto the $\\mathbb{R}^3$ space:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_3D_spectral_demo(transformation_matrix=A, eigenbasis_matrix=V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They should look the same!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Your Turn! (Break down a 3 by 3 matrix into its components)\n",
    "\n",
    "Now, it's your turn to break down a $3\\times3$ matrix into its components. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.array(\n",
    "    [\n",
    "        [1.42297122, 0.3570813, 0.56819933],\n",
    "        [0.3570813, 1.73182147, 0.29192651],\n",
    "        [0.56819933, 0.29192651, 1.84520731],\n",
    "    ]\n",
    ")\n",
    "print(\"A:\\n{}\".format(stringify_matrix(A)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let's calculate some eigen-things. We'll let you use the `np.linalg` package.\n",
    "\n",
    "**Hint:** look into `np.linalg.eig`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s, V = np.linalg.eig(A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's try plotting the parallelepiped!\n",
    "\n",
    "First let's plot the $\\mathbb{R}^3$ region with the identity function applied, with the elementary basis vectors aligned to the $x-$, $y-$, and $z-$axes. Also plot the eigenvectors $v_1, v_2, v_3$.\n",
    "\n",
    "_**Hint:** Which set of vectors are aligned with the $x-$,$y-$, and $z-$axes?_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T1 =   # SOLUTION\n",
    "\n",
    "# BEGIN SOLUTION\n",
    "\n",
    "# END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's apply $V^T$. What is the function now? Is there any rotation? How would you represent this function as a matrix?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T2 =   # SOLUTION\n",
    "\n",
    "# BEGIN SOLUTION\n",
    "\n",
    "# END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's do some stretching! Apply $S$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T3 =  # SOLUTION\n",
    "\n",
    "# BEGIN SOLUTION\n",
    "\n",
    "# END SOLUTION"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, apply $V$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T4 =   # SOLUTION\n",
    "\n",
    "# BEGIN SOLUTION\n",
    "\n",
    "# END SOLUTION"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
