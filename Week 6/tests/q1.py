test = {   'name': 'q1',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> \n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> ## DO NOT MODIFY BELOW '
                                               'THIS COMMENT ##;\n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> \n'
                                               '>>> num_iters, min_n, max_n, '
                                               'sigma = 1000, 10, 100, 10;\n'
                                               '>>> for _ in '
                                               'range(num_iters):\n'
                                               '...     n = '
                                               'np.random.randint(min_n, '
                                               'max_n)\n'
                                               '...     v = sigma * '
                                               'np.random.randn(n)\n'
                                               '...     u = normalize(v)\n'
                                               '...     d = np.linalg.norm(u)\n'
                                               '...     assert math.isclose(\n'
                                               '...         d, 1\n'
                                               '...     ), "Vector after '
                                               'normalization has L_2 norm {}, '
                                               'when it should have norm '
                                               '1.".format(\n'
                                               '...         d\n'
                                               '...     );\n'
                                               '>>> print("Congratulations! '
                                               'Your normalize function is '
                                               'correct!")\n'
                                               'Congratulations! Your '
                                               'normalize function is '
                                               'correct!\n',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
