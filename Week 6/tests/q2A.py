test = {   'name': 'q2A',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> \n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> ## DO NOT MODIFY BELOW '
                                               'THIS COMMENT ##;\n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> \n'
                                               '>>> num_iters, min_n, max_n, '
                                               'sigma = 1000, 10, 100, 10;\n'
                                               '>>> for _ in '
                                               'range(num_iters):\n'
                                               '...     n = '
                                               'np.random.randint(min_n, '
                                               'max_n)\n'
                                               '...     v1 = sigma * '
                                               'np.random.randn(n)\n'
                                               '...     v2 = sigma * '
                                               'np.random.randn(n)\n'
                                               '...     s = vec_vec_inner(v1, '
                                               'v2)\n'
                                               '...     p = np.dot(v1, v2)\n'
                                               '...     assert math.isclose(\n'
                                               '...         s, p\n'
                                               '...     ), "Calculated value '
                                               '({}) does not match the '
                                               'expected value ({})".format(s, '
                                               'p);\n'
                                               '>>> print("Congratulations! '
                                               'Your vec_vec_inner function is '
                                               'correct!")\n'
                                               'Congratulations! Your '
                                               'vec_vec_inner function is '
                                               'correct!\n',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
