test = {   'name': 'q3',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> random_number = '
                                               'np.random.rand(10);\n'
                                               '>>> '
                                               'np.all(np.isclose(coordinatewise_exponential(random_number), '
                                               'np.exp(random_number)))\n'
                                               'True',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
