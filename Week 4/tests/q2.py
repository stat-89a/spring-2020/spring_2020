test = {   'name': 'q2',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> random_number = '
                                               'np.random.rand(10);\n'
                                               '>>> '
                                               'np.all(np.isclose(coordinatewise_sin(random_number), '
                                               'np.sin(random_number)))\n'
                                               'True',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
