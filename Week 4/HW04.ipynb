{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "# Initialize OK\n",
    "from client.api.notebook import Notebook\n",
    "ok = Notebook('HW04.ok')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Don't change this cell; just run it.\n",
    "import numpy as np\n",
    "import math\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from linalg_for_datasci.widgets import (\n",
    "    p_ball_widget,\n",
    "    init_updating_plot,\n",
    "    updating_ball_to_box_ratio_plot,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.style.use(\"fivethirtyeight\")\n",
    "%matplotlib widget"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Unit $p$-balls in $\\mathbb{R}^2$ for different values of $p$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Recall that in $\\mathbb{R}^2$, the unit $p$-ball is the set $\\mathbb{B}_p = \\{x = (x_1,x_2)\\in\\mathbb{R}^2 : \\|x\\|_p \\leq 1\\}$. In this problem, we will visualize what these balls look like for different values of $p$. For this problem, most of the work is already done for you; the only thing you need to do is fill in the function `dart_did_land_in_p_ball_R2`, which takes in a number $p>0$, and should select a point $x\\in [-1,1]\\times [-1,1]$ at random, and return a python dictionary with two keys: \"coordinates\", containing the coordinates (in a numpy array) of the randomly drawn point, and \"in_ball\", which is `True` if the point is in the set $\\mathbb{B}_p$, and `False` if it is not. We can think about this as throwing a 'dart' at the box defined by $[-1,1]\\times[-1,1]$, and recording whether or not it landed in the $p$-ball. For example, a sample output would be:\n",
    "```\n",
    "dart_did_land_in_p_ball_R2(p=2)\n",
    "{'coordinates': array([-0.28393453, -0.90275026]), 'in_ball': True}\n",
    "dart_did_land_in_p_ball_R2(p=2)\n",
    "{'coordinates': array([-0.81386644,  0.97708726]), 'in_ball': False}\n",
    "```\n",
    "\n",
    "Here are some functions you might find helpful in completing this question: \n",
    "\n",
    "`np.linalg.norm`  \n",
    "`np.random.rand`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<!--\n",
    "BEGIN QUESTION\n",
    "name: q1\n",
    "manual: false\n",
    "points: 1\n",
    "-->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dart_did_land_in_p_ball_R2(p):\n",
    "    ...\n",
    "    dart = {\"coordinates\": coordinates, \"in_ball\": in_ball}\n",
    "    return dart"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "ok.grade(\"q1\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Demonstration: Visualizing the unit $p$-ball\n",
    "\n",
    "We can create a scatterplot showing only those points that landed within the unit ball to create a visualization of what the unit ball looks like for various norms in $\\mathbb{R}^2$.\n",
    "\n",
    "Try various values of $p$ using the slider below for a graphical visualization of the $L_p$ unit ball in $\\mathbb{R}^2$.\n",
    "\n",
    "\n",
    "Note that `p` doesn't even have to be an integer! Any value such that `p >= 1` works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p_ball_widget(dart_did_land_in_p_ball_R2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What appears to be happening as the value of `p` increases?\n",
    "\n",
    "Notice also that all of these shapes are convex, meaning that for any two points $x,y\\in \\mathbb{B}_p$, the line connecting $x$ and $y$ is entirely contained in $\\mathbb{B}_p$. We will see in 1.3 that this is not the case when we consider values of $p$ less than 1. Unit balls are convex **if and only if** the measure of distance satisfies the triangle inequality."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Demonstration: Limit of $p$\n",
    "\n",
    "Recall that the infinity norm $\\|x\\|_{\\infty} = \\max_{i} |x_i|$ can be obtained as a limit $\\lim_{p\\rightarrow\\infty}\\|x\\|_p$. Notice that the unit $\\infty$-ball is really just the entire box $[-1,1]\\times [-1,1]$. In 1.1, we visualized what the unit $p$-balls look like for various values of $p$. Here, we will empirically observe that as $p\\rightarrow\\infty$, the area of the unit $p$-ball approaches the area of the unit $\\infty$-ball. Running the following two lines visualizes the ratio $\\text{Area}(\\mathbb{B}_p)/\\text{Area}(\\mathbb{B}_\\infty)$ as $p$ becomes large."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_widget, figure, axis = init_updating_plot(\n",
    "    title=\"Area of unit $p$-ball divided by area of unit box in $\\mathbb{R}^2$\",\n",
    "    xlabel=\"$p$\",\n",
    "    ylabel=\"ratio\",\n",
    "    xlim=(0, 100),\n",
    "    ylim=(0, 1.1),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's plot the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "updating_ball_to_box_ratio_plot(dart_did_land_in_p_ball_R2, output_widget, figure, axis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Values of $p$ which don't correspond to norms\n",
    "\n",
    "Values of $p$ such that $0 < p < 1$ don't lead to valid norm functions, but they are still well-defined. Let's look at them below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "p_ball_widget(dart_did_land_in_p_ball_R2, concave=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how all of the shapes above are concave (except when $p=1$). Thus for $0 < p <1$, the triangle inequality is not satisfied, and these are not actually norms!\n",
    "\n",
    "For really small values of $p$, so few points land inside of the ball that there's often nothing to plot!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from linalg_for_datasci.tests import typecheck_vector, typecheck_series\n",
    "from linalg_for_datasci.plotting import plot_circular_region_with_func_random_pts\n",
    "from linalg_for_datasci.widgets import scalar_multiplication_widget, addition_widget\n",
    "\n",
    "%matplotlib inline\n",
    "plt.style.use(\"fivethirtyeight\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Visualizing Linear Functions\n",
    "\n",
    "Being a linear function involves \"preserving\" or \"commuting with\" the two fundamental operations of vector spaces: addition and scalar multiplication.\n",
    "\n",
    "Specifically, if $V$ and $W$ are two vector spaces, then a function $f: V \\to W$ is _**linear**_ if and only if:\n",
    "\n",
    "- For any vector $\\mathbf{v} \\in V$ and any real number (scalar) $a \\in \\mathbb{R}$, the function $f$ \"preserves\" i.e. \"commutes with\" scalar multiplication:\n",
    "    $$f (a \\cdot \\mathbf{v}) = a \\cdot f (\\mathbf{v})  \\,.$$\n",
    "- For any two vectors $\\mathbf{v}_1, \\mathbf{v}_2 \\in V$, the function $f$ \"preserves\" i.e. \"commutes with\" addition:\n",
    "    $$ f ( \\mathbf{v}_1 + \\mathbf{v}_2)  = f(\\mathbf{v}_1) + f(\\mathbf{v}_2) \\,.$$\n",
    "\n",
    "In the cells below, we will try to visualize the effects that certain functions $f: \\mathbb{R}^2 \\to \\mathbb{R}^2$ have on vectors, and what it looks like when they are linear, and what it looks like when they aren't."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Functions that preserve neither addition nor scalar multiplication\n",
    "\n",
    "## 2.1.1\n",
    "\n",
    "In the cell below, write a lambda function which, given as input a vector, returns a new vector whose entries are the values of `np.sin` applied to the entries of the original vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<!--\n",
    "BEGIN QUESTION\n",
    "name: q2\n",
    "manual: false\n",
    "points: 1\n",
    "-->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinatewise_sin = ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "ok.grade(\"q2\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function transforms the unit disc into a shape which is symmetric about the origin."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(coordinatewise_sin)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However, we will see that this function not only fails to preserve addition, it also fails to preserve multiplication too. So transforming the unit disc into a shape which is symmetric about the origin is not enough by itself to guarantee that either of the two operations is preserved."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below shows how this function fails to preserve scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(coordinatewise_sin)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below shows how this function also fails to preserve addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(coordinatewise_sin)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1.2\n",
    "\n",
    "Most functions $f: \\mathbb{R}^2 \\to \\mathbb{R}^2$ fail to preserve either scalar multiplication or addition.\n",
    "\n",
    "As another example, write a lambda function which returns a new vector whose entries are the values of `np.exp` applied to the entries of the original vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "<!--\n",
    "BEGIN QUESTION\n",
    "name: q3\n",
    "manual: false\n",
    "points: 1\n",
    "-->"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "coordinatewise_exponential = ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "ok.grade(\"q3\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how this function transforms the unit disc into a region with no symmetry about the origin:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(coordinatewise_exponential)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The cell below demonstrates how this function fails to preserve scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(coordinatewise_exponential)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also visualize how this function fails to preserve addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(coordinatewise_exponential)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Functions that preserve scalar multiplication, but not addition\n",
    "\n",
    "A function that preserves scalar multiplication but not addition \"acts like a linear function\" on every line through the origin, but it isn't consistent about what it does to different lines. Therefore we need at least a two-dimensional vector space to find a counterexample, since a one-dimensional vector space only has one line through the origin.\n",
    "\n",
    "## 2.2.1\n",
    "\n",
    "Here is one example of such a function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import circle_to_diamond"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The image under this function of any circle centered at the origin is a diamond (i.e. a rhombus)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(circle_to_diamond)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see in the cell below how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(circle_to_diamond)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This cell shows that the function does _not_ preserve addition. Even though $f(\\mathbf{v}_1) + f(\\mathbf{v}_2)$ and $f(\\mathbf{v}_1 + \\mathbf{v}_2)$ seem to always point in the same direction, in general they have different lengths."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(circle_to_diamond)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2.2\n",
    "\n",
    "Any function that preserves scalar multiplication corresponds to a distortion of the unit circle to a shape which is symmetric with respect to reflection about the origin. We saw this with the function above, which transformed the unit circle into a diamond. \n",
    "\n",
    "Linear functions also preserve scalar multiplication. A linear functions will always distort the unit circle to an ellipse, a line segment, or for the all-zero linear function a single point at the origin, all of which are symmetric with respect to reflection about the origin. \n",
    "\n",
    "Thus any function which preserves scalar multiplication but which does not preserve addition must distort the unit circle to a more exotic shape that is symmetric with respect to reflection about the origin. (The converse isn't true, see the above example of coordinate-wise sine.)\n",
    "\n",
    "Here is an example of another function which preserves scalar multiplication but not addition. Notice how it transforms the unit disc into a (fairly strange) shape which has symmetry with respect to reflection about the origin."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import circle_to_double_shell"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is what it does to the unit disc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(circle_to_double_shell)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see in the cell below how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(circle_to_double_shell)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This cell shows that the function does _not_ preserve addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(circle_to_double_shell)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.3 Functions that preserve addition, but not scalar multiplication\n",
    "\n",
    "There are no continuous functions which have this property for real vector spaces. (This is because the dyadic rationals are a dense subset of the real numbers, but don't worry about that.) Real vector spaces are the type of vector spaces we will focus on in this course: their scalars are real numbers.\n",
    "\n",
    "However, there is a continuous function which preserves addition, but not scalar multiplication, for the complex plane $\\mathbb{C}$ considered as a complex vector space, i.e. a vector space whose scalars are complex numbers. Specifically, the function:\n",
    "$$ f: a + bi \\mapsto a - bi$$\n",
    "preserves addition, i.e. $f(\\mathbf{z}_1 + \\mathbf{z}_2) = f(\\mathbf{z}_1) + f(\\mathbf{z}_2)$, but it does not preserve scalar multiplication, e.g. if the scalar $c = i$, and the \"vector\" $\\mathbf{z} = 2 + 3i$, then $f(\\mathbf{z}) = 2 - 3i$, $c \\cdot \\mathbf{z} = -3 + 2i$, so that  $f(c \\cdot \\mathbf{z}) = -3 - 2i$, but $c \\cdot f(\\mathbf{z}) = 3 + 2i$. This function is called taking the \"conjugate\" of the complex number.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4 Functions that preserve addition and scalar multiplication: linear functions\n",
    "\n",
    "While most functions are not linear functions, there are many different types of linear function. Here we review some of them below.\n",
    "\n",
    "## 2.4.1 Rotation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function rotates any vector in $\\mathbb{R}^2$ counterclockwise by $45^\\circ$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import rotate_45_ccw"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This function does not change the unit disc very much, since the unit disc is invariant under rotation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(rotate_45_ccw)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how the resulting shape is symmetric about the origin, as it should be for any function that preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(rotate_45_ccw)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(rotate_45_ccw)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4.2 Shear"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following function shears parallel to the vertical ($x_2$) axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import shear_vertically"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how this function changes the unit disc into an ellipse, the way we said that linear functions change circles to ellipses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(shear_vertically)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In particular notice again how this shape is symmetric about the origin. \n",
    "\n",
    "(Remember again that the shape being symmetric about the origin is necessary, but not sufficient, for the function to preserve scalar multiplication.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(shear_vertically)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(shear_vertically)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4.3 Reflection\n",
    "\n",
    "The following function reflects any vector in $\\mathbb{R}^2$ about the vertical ($x_2$) axis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import reflect_over_vertical_axis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The unit disc is invariant under reflection over the vertical axis, which means that this function does not change the unit disc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(reflect_over_vertical_axis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, this shape is a (degenerate) ellipse which symmetric about the origin."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(reflect_over_vertical_axis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(reflect_over_vertical_axis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.4.4 Projection\n",
    "\n",
    "The following function takes any vector in $\\mathbb{R}^2$ and projects it to the closest point on the $x_1 = x_2$ line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.linear_func import projection_onto_diagonal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function changes the unit disc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot_circular_region_with_func_random_pts(projection_onto_diagonal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice how the resulting shape (a line segment, i.e. a \"degenerate ellipse\") is again symmetric about the origin."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves scalar multiplication."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scalar_multiplication_widget(projection_onto_diagonal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following cell demonstrates how this function preserves addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "addition_widget(projection_onto_diagonal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since scalar multiplication and addition are the fundamental operation on vector spaces, functions which preserve both, i.e. linear functions, are of fundamental importance to linear algebra. (Arguably \"vector spaces\" should probably be called \"linear spaces\", but social convention says otherwise.) In this course we will discuss at length about how we can use matrices to represent linear functions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "source": [
    "# Submit\n",
    "Make sure you have run all cells in your notebook in order before running the cell below, so that all images/graphs appear in the output.\n",
    "**Please save before submitting!**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "deletable": false,
    "editable": false
   },
   "outputs": [],
   "source": [
    "# Save your notebook first, then run this cell to submit.\n",
    "ok.submit()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
