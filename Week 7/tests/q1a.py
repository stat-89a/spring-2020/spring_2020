test = {   'name': 'q1a',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> \n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> ## DO NOT MODIFY BELOW '
                                               'THIS COMMENT ##;\n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> pure_sin = np.sin(2 * '
                                               'math.pi * xs / d);\n'
                                               '>>> pure_exp = np.exp(-xs / 2 '
                                               '* math.pi);\n'
                                               '>>> '
                                               'np.all([proj_coef(sin_funcs[:, '
                                               '0], pure_sin) > 0.8,  '
                                               'proj_coef(sin_funcs[:, 0], '
                                               'pure_exp) < 0.5])\n'
                                               'True',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
