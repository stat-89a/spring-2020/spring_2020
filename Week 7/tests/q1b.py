test = {   'name': 'q1b',
    'points': 1,
    'suites': [   {   'cases': [   {   'code': '>>> \n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> ## DO NOT MODIFY BELOW '
                                               'THIS COMMENT ##;\n'
                                               '>>> '
                                               '######################################;\n'
                                               '>>> np.all([sin_acc / '
                                               'sin_funcs.shape[1] > 0.8, \n'
                                               '...         exp_acc / '
                                               'exp_funcs.shape[1] > 0.8])\n'
                                               'True',
                                       'hidden': False,
                                       'locked': False}],
                      'scored': True,
                      'setup': '',
                      'teardown': '',
                      'type': 'doctest'}]}
