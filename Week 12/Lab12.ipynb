{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "is_imports_cell": true
   },
   "outputs": [],
   "source": [
    "# Run this cell, please do not change\n",
    "%matplotlib inline\n",
    "from scipy import stats\n",
    "import statsmodels.api as sm\n",
    "import numpy as np\n",
    "import math\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import sklearn\n",
    "\n",
    "plt.style.use(\"fivethirtyeight\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lab 12: Least Squares Regression\n",
    "\n",
    "In this assignment, we investigate least squares regression when we have many predictors $X_1, X_2, \\dots, X_p$, associated with a response $y$ which we hope to predict.\n",
    "\n",
    "Recall that the standard linear regression model takes the form\n",
    "\n",
    "$$\n",
    "y = b_0 + b_1 X_1 + b_2 X_2 + \\cdots +  b_p X_p +\\epsilon\n",
    "$$\n",
    "\n",
    "where $\\epsilon \\sim N(0,\\sigma^2)$ is a noise variable. Our objective is to estimate the coefficients $b_0,b_1,\\dots,b_p$ using a dataset of $n$ examples $\\mathbf{x}_1,\\dots,\\mathbf{x}_n$, $y_1,\\dots,y_n$, where $\\mathbf{x}_i \\in \\mathbb{R}^p$. We can collect the data into an $n\\times p$ matrix $\\mathbf{X}$ (whose $i^{th}$ row is the sample, $\\mathbf{x}_i$) and a vector $\\mathbf{y} = \\begin{pmatrix} y_1\\\\ \\vdots \\\\ y_n\\end{pmatrix}$. In least squares regression, we find estimated parameters $\\hat{b}_0, \\hat{b}_1, \\dots, \\hat{b}_p$ that minimize the sum of the squared errors:\n",
    "\n",
    "$$\n",
    "SSE = \\sum_{i=1}^n (\\hat{b}_0 + \\hat{b}_1 \\mathbf{X}_{i,1} + \\cdots + \\hat{b}_p \\mathbf{X}_{i,p} - y_i)^2\n",
    "$$\n",
    "\n",
    "We can write this a bit more succinctly by using the following convention: let's append a column of $1$'s to the matrix $\\mathbf{X}$ (so now it's a $n \\times (p+1)$ matrix), and collect the coefficients $\\hat{b}_0,\\dots,\\hat{b}_p$ into a vector $\\hat{\\mathbf{b}} = \\begin{pmatrix} \\hat{b}_0\\\\ \\vdots \\\\ \\hat{b}_p\\end{pmatrix} \\in \\mathbb{R}^{p+1}$. Then the sum of squared error can be written as:\n",
    "\n",
    "$$\n",
    "SSE = \\|\\mathbf{X}\\hat{\\mathbf{b}} - \\mathbf{y}\\|_2^2\n",
    "$$\n",
    "\n",
    "Using some calculus, we can find that the vector $\\hat{\\mathbf{b}}$ that minimizes the SSE satisfies the so-called normal equations:\n",
    "$$\n",
    "\\mathbf{X}^\\top \\mathbf{X} \\hat{\\mathbf{b}} = \\mathbf{X}^\\top \\mathbf{y}\n",
    "$$\n",
    "\n",
    "In the next problem, we investigate a few ways to solve these equations for the parameter vector $\\hat{\\mathbf{b}}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem 1. Three methods for solving the least squares problem\n",
    "\n",
    "Below is a toy data set with one response `y` and three predictors `x1`, `x2`, `x3`. We will use this to test the three different methods for solving the least squares problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x1 = np.array([5, 3, 4, 1, 3])\n",
    "x2 = np.array([3, 3, 3, 4, 9])\n",
    "x3 = np.array([3.2, 1.3, 1.9, 2.1, 3])\n",
    "y = np.array([1, 2, 5, 3, 2])\n",
    "\n",
    "n = len(x1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Add an intercept column of all ones\n",
    "We can form the data matrix whose first column is the all one's vector (the intercept column), and whose remaining three columns correspond to the variables `x1`, `x2`, `x3` with the following code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.column_stack([np.ones(n), x1, x2, x3]) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.2 Solving the least squares problem directly by inverting $\\mathbf{X}^\\top \\mathbf{X}$\n",
    "\n",
    "Let us return to the normal equations defining the parameter vector $\\hat{\\mathbf{b}}$:\n",
    "\n",
    "$$\n",
    "\\mathbf{X}^\\top \\mathbf{X} \\hat{\\mathbf{b}} = \\mathbf{X}^\\top \\mathbf{y}.\n",
    "$$\n",
    "\n",
    "A simple way to solve for $\\hat{\\mathbf{b}}$ is by left-multiplying each side by $(\\mathbf{X}^\\top \\mathbf{X})^{-1}$, which gives\n",
    "\n",
    "$$\n",
    "\\hat{\\mathbf{b}} = (\\mathbf{X}^\\top \\mathbf{X})^{-1}\\mathbf{X}^\\top \\mathbf{y}\n",
    "$$\n",
    "\n",
    "In the cell below, fill in the function ```solve_ls```, which takes in a matrix $\\mathbf{X}$ and vector $\\mathbf{y}$ and returns the parameters $\\hat{\\mathbf{b}}$ using this method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_ls(X,y):\n",
    "    '''\n",
    "    X: n x (p+1) array of predictor variables\n",
    "    y: length n vector of respones\n",
    "    '''\n",
    "    return # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use this function to find the least squares parameters $\\hat{\\mathbf{b}}$ for the toy data we made above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hat = # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.3 Solving the normal equations using QR decomposition.\n",
    "One downside of the above approach is that it requires finding the inverse of the matrix $\\mathbf{X}^\\top\\mathbf{X}$, which can be quite costly when $p$ is large. In this problem, we introduce a more efficient way of finding $\\hat{\\mathbf{b}}$ using the QR decomposition. \n",
    "\n",
    "Recall that we can decompose the matrix $\\mathbf{X}$ as $\\mathbf{X} = \\mathbf{Q}\\mathbf{R}$, where $\\mathbf{Q}$ is an orthogonal matrix and $\\mathbf{R}$ is upper triangular. Then from the normal equations, we have\n",
    "\n",
    "$$\n",
    "(\\mathbf{X}^\\top \\mathbf{X}) \\hat{\\mathbf{b}} = \\mathbf{X}^\\top \\mathbf{y} \\iff ((\\mathbf{Q}\\mathbf{R})^\\top \\mathbf{Q}\\mathbf{R}) \\hat{\\mathbf{b}} = (\\mathbf{Q}\\mathbf{R})^\\top \\mathbf{y} \\iff \\mathbf{R}^\\top \\mathbf{R} \\hat{\\mathbf{b}} = \\mathbf{R}^\\top \\mathbf{Q}^\\top \\mathbf{y}\n",
    "$$\n",
    "Now, if we left-multiply both sides by $(\\mathbf{R}^\\top \\mathbf{R})^{-1}$, we get a new expression for $\\hat{\\mathbf{b}}$:\n",
    "$$\n",
    "\\hat{\\mathbf{b}} = (\\mathbf{R}^\\top \\mathbf{R})^{-1}\\mathbf{R}^\\top\\mathbf{Q}^\\top \\mathbf{y} = \\mathbf{R}^{-1}\\mathbf{Q}^\\top \\mathbf{y}\n",
    "$$\n",
    "Fill in the function ```solve_ls_qr``` to find the least squares coefficients $\\hat{\\mathbf{b}}$ using this method. Recall that we can get the QR decomposition of a matrix using the function ```np.linalg.qr```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_ls_qr(X,y):\n",
    "    '''\n",
    "    X: n x (p+1) array of predictor variables\n",
    "    y: length n vector of respones\n",
    "    '''\n",
    "    # YOUR CODE GOES HERE\n",
    "    # YOUR CODE GOES HERE\n",
    "    # YOUR CODE GOES HERE\n",
    "    return # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use this function to find the least-squares parameters $\\hat{\\mathbf{b}}'$ for the toy data, and verify that you get the same answer that from the function ```solve_ls```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hat_qr = # YOUR CODE GOES HERE\n",
    "np.allclose(b_hat, b_hat_qr) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.4 Solving the least squares problem with QR and backsubstitution\n",
    "\n",
    "Let us return to the equation we obtain from plugging the QR decomposition into the normal equations\"\n",
    "\n",
    "$$\n",
    "\\mathbf{R}^\\top \\mathbf{R} \\hat{\\mathbf{b}} = \\mathbf{R}^\\top \\mathbf{Q}^\\top \\mathbf{y}\n",
    "$$\n",
    "\n",
    "In the previous problem, we left-multiplied both sides by $(\\mathbf{R}^\\top\\mathbf{R})^{-1}$. Here, we show that we can find the solution vector $\\hat{\\mathbf{b}}$ without needing to invert any matrices at all! This can be useful when the matrices $\\mathbf{X}$ or $\\mathbf{R}$ have some degeneracies that make numerical inversion difficult.\n",
    "\n",
    "From the above equation, let's multiply both sides by $(\\mathbf{R}^\\top)^{-1}$, and define $\\mathbf{z} = \\mathbf{Q}^\\top \\mathbf{y}$ to get the new equations\n",
    "\n",
    "$$\n",
    "\\mathbf{Rb} = \\mathbf{Q}^\\top \\mathbf{y} = \\mathbf{z}\n",
    "$$\n",
    "\n",
    "This set of equations is more attractive from a computational perspective specifically because the matrix $\\mathbf{R}$ is a triangular matrix. This means that we can use a method called [back-substitution](http://www.tutor-homework.com/Math_Help/college_algebra/m5l4notes.pdf) to solve the system. We can use the function ```solve_triangular``` included in the scipy library (imported below) to solve a system using this method. This function takes two arguments: a triangular $\\mathbf{R}$ and a vector $\\mathbf{z}$, and returns the vector $\\mathbf{b}$ satisfying \n",
    "$$\n",
    "\\mathbf{Rb} = \\mathbf{z}.\n",
    "$$\n",
    "Fill in the function ```solve_ls_backsub()```, which again takes in a matrix $\\mathbf{X}$ and vector $\\mathbf{y}$, and returns the coefficients $\\hat{\\mathbf{b}}$ using the backsubstitution method. Recall that we can use the function ```np.linalg.qr``` to compute the QR decompostion of matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.linalg import solve_triangular\n",
    "\n",
    "\n",
    "def solve_ls_backsub(X,y):\n",
    "    '''\n",
    "    X: n x (p+1) array of predictor variables\n",
    "    y: length n vector of respones\n",
    "    '''\n",
    "    # YOUR CODE GOES HERE\n",
    "    # YOUR CODE GOES HERE\n",
    "    # YOUR CODE GOES HERE\n",
    "    return # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell below, verify that this function gives you the same solution as the previous two methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hat_bs = # YOUR CODE GOES HERE\n",
    "np.allclose(b_hat, b_hat_bs)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.5 Testing the performance difference between the three methods (no coding needed)\n",
    "\n",
    "In this part, we will test the speed of the three methods implemented above, and compare the time each takes to compute $\\hat{\\mathbf{b}}$ for different values of $n$ and $p$. \n",
    "\n",
    "First, we'll define a function that will generate some random data for us with given values of $n$ and $p$. Note that the data here will tend to be 'well-conditioned', and so the results won't necessarily generalize to every possible situation. Still, they give us a rough idea of the efficiency of each of the methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_random_data(n,p):\n",
    "    X = np.random.normal(size=(n,p))\n",
    "    W = np.random.normal(size=p)\n",
    "    eps = np.random.normal(size=n)\n",
    "    y = np.dot(X,W) + eps\n",
    "    return X, y"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we will use this function to generate a bunch of fake datasets $\\mathbf{X}$ and $\\mathbf{y}$, and calculate how long it takes to compute the least squares coefficients. For this, we'll fix the number of samples $n$ at 500."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "\n",
    "normal_ls_times = []\n",
    "qr_ls_times = []\n",
    "bs_ls_times = []\n",
    "\n",
    "for p in np.arange(10, 450, 10):\n",
    "    X, y = make_random_data(n=500, p=p)\n",
    "    \n",
    "    tic1 = time.time()\n",
    "    solve_ls(X,y)\n",
    "    normal_ls_times.append(time.time()-tic1)\n",
    "    \n",
    "    tic2 = time.time()\n",
    "    solve_ls_qr(X,y)\n",
    "    qr_ls_times.append(time.time()-tic2)\n",
    "    \n",
    "    tic3 = time.time()\n",
    "    solve_ls_backsub(X,y)\n",
    "    bs_ls_times.append(time.time()-tic3)\n",
    "    \n",
    "plt.plot(np.arange(10, 450, 10), normal_ls_times, label='direct method')\n",
    "plt.scatter(np.arange(10, 450, 10), normal_ls_times)\n",
    "plt.plot(np.arange(10, 450, 10), bs_ls_times, label='backsub method')\n",
    "plt.scatter(np.arange(10, 450, 10), bs_ls_times)\n",
    "plt.plot(np.arange(10, 450, 10), qr_ls_times, label='qr method')\n",
    "plt.scatter(np.arange(10, 450, 10), qr_ls_times)\n",
    "\n",
    "plt.xlabel('Number of features, p', fontsize=16)\n",
    "plt.ylabel('Seconds', fontsize=16)\n",
    "plt.legend()\n",
    "plt.title('Amount of time needed to compute LS coefficients vs. p', fontsize=16)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problem 2. Linear Regression on Boston Housing data\n",
    "\n",
    "Let's apply what we've learned to an actual dataset, the Boston housing dataset, which is loaded below. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "# Run this cell without changing anything.\n",
    "from sklearn.datasets import load_boston\n",
    "boston_dataset = load_boston()\n",
    "\n",
    "X = boston_dataset.data\n",
    "y = boston_dataset.target\n",
    "X = np.delete(X, [11], axis=1)\n",
    "n,p = X.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we see, this dataset has $p=12$ predictor variables, and $n=506$ samples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.1 Adding a column of ones\n",
    "\n",
    "In the cell below, append a column of 1's to the matrix $\\mathbf{X}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.2 Computing the least squares coefficients\n",
    "\n",
    "Use any of the methods you defined in Problem 1 to compute the least squares coefficients $\\hat{\\mathbf{b}}$ for this problem."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b_hat = # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.3 Plotting residuals\n",
    "\n",
    "In this problem, we look at one important way to visualize the the quality of the fit from least squares using the _residuals_. The residuals are defined as the difference between the predicted $y$ values and the true $y$ values:\n",
    "\n",
    "$$\n",
    "\\mathbf{e} = \\mathbf{y} - \\hat{\\mathbf{y}} = \\mathbf{y} - \\mathbf{X}\\hat{\\mathbf{b}}\n",
    "$$\n",
    "\n",
    "In the cell below, fill in the function ```compute_residuals``` to compute this vector of residuals for a given data matrix $\\mathbf{X}$, response vector $\\mathbf{y}$ and coefficient vector $\\hat{\\mathbf{b}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_residuals(X, y, b):\n",
    "    '''\n",
    "    X: n x (p+1) array of predictor variables\n",
    "    y: length n vector of respones\n",
    "    b: length (p+1) vector of regression coefficients\n",
    "    '''\n",
    "    # YOUR CODE GOES HERE\n",
    "    return # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's use this function to plot the residuals:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_residuals(X, y, b):\n",
    "    residuals = compute_residuals(X, y, b)\n",
    "    fitted_values = np.dot(X,b)\n",
    "    plt.scatter(fitted_values, residuals)\n",
    "    plt.xticks(())\n",
    "    plt.xlabel('Fitted values', fontsize=16)\n",
    "    plt.ylabel('Residuals', fontsize=16)\n",
    "    plt.show()\n",
    "    \n",
    "plot_residuals(X, y, b_hat)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To interpret the residual plot, let's first recall our regression model:\n",
    "\n",
    "$$\n",
    "y = b_0 + b_1 X_1 + b_2 X_2 + \\cdots +  b_p X_p +\\epsilon\n",
    "$$\n",
    "\n",
    "where $\\epsilon \\sim N(0,\\sigma^2)$ is a noise variable. If we plug in the fitted coefficients $\\hat{b}_i$ and subtract the fitted values from each side, we get that\n",
    "\n",
    "$$\n",
    "y - (\\hat{b}_0 + \\hat{b}_1 X_1 + \\hat{b}_2 X_2 + \\cdots +  \\hat{b}_p X_p) = \\epsilon\n",
    "$$\n",
    "\n",
    "Thus, if our model is correct, we expect the residuals to be roughly normaly with mean zero. As we see from the plot above, the residuals do seem to be centered around zero. However, it's not clear from this plot alone that they actually follow a normal distribution. Let's look at the histogram of the residuals to get a slightly better picture of their distribution.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "residuals = compute_residuals(X, y, b_hat)\n",
    "plt.hist(residuals, bins=50)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this plot, we see that the residuals do roughly fit a bell curve shape of a normal distribution, but it's not quite clear how well. In the next section, we investigate a more refined way to assess how well the residuals follow a normal distribution."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.4 QQ plots\n",
    "\n",
    "A QQ (quantile-quantile) plot is a popular way to assess how well two distributions match. If you're familiar with some probability, you can read more about QQ plots on the [Wikipedia page](https://en.wikipedia.org/wiki/Q–Q_plot). For now, it will suffice to know that it's a convenient way to visualize how well two distributions match.\n",
    "\n",
    "In our case, we want to see how well the residuals follow a $N(0,\\sigma^2)$ distribution. To do this, we first need to know the variance $\\sigma^2$. We can estimate this from our residuals using the following formula:\n",
    "\n",
    "$$\n",
    "\\hat{\\sigma}^2 = \\frac{1}{n-2}(\\mathbf{y}-\\hat{\\mathbf{y}})^\\top(\\mathbf{y} - \\hat{\\mathbf{y}}) = \\frac{1}{n-2}(\\mathbf{y}-\\mathbf{X}\\hat{\\mathbf{b}})^\\top(\\mathbf{y} - \\mathbf{X}\\hat{\\mathbf{b}})\n",
    "$$\n",
    "\n",
    "Fill in the function ```estimate_residual_variance``` below to compute $\\hat{\\sigma}^2$ below (hint: you can use your ```compute_residuals``` function you defined above)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def estimate_residual_variance(X, y, b):\n",
    "    '''\n",
    "    X: n x (p+1) array of predictor variables\n",
    "    y: length n vector of respones\n",
    "    b: length (p+1) vector of regression coefficients\n",
    "    '''\n",
    "    # YOUR CODE GOES HERE\n",
    "    # YOUR CODE GOES HERE\n",
    "    return # YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The QQ plot compares two distributions by measuring how much their _quantiles_ align. We do this by plotting the quantiles of the residual distribution versus the quantiles of a normal distribution. Ideally, these would match exactly, and we would have that the quantiles lie on exactly on the line $y=x$. The following function uses your ```estimate_residual_variance``` and ```compute_residuals``` functions to generate a QQ plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.stats import norm\n",
    "\n",
    "def residual_quantile(q, residuals):\n",
    "    return np.quantile(residuals, q)\n",
    "\n",
    "def QQ_plot(X, y, b):\n",
    "    residuals = compute_residuals(X, y, b)\n",
    "    sigma2 = estimate_residual_variance(X, y, b)\n",
    "    q_range = np.arange(0.01,1,.01)\n",
    "    qq_data = []\n",
    "    for q in q_range:\n",
    "        res_q = residual_quantile(q, residuals)\n",
    "        norm_q = norm.ppf(q, scale=np.sqrt(sigma2))\n",
    "        qq_data.append([norm_q, res_q])\n",
    "    \n",
    "    qq_data = np.array(qq_data)\n",
    "    x_lb = np.min(qq_data)\n",
    "    x_ub = np.max(qq_data)\n",
    "    xran = np.arange(x_lb,x_ub)\n",
    "    plt.plot(qq_data[:,0], qq_data[:,1])\n",
    "    plt.plot(xran, xran, color='black', linestyle='--', linewidth=.5, label='y=x')\n",
    "    plt.legend()\n",
    "    plt.ylabel('Residual quantiles', fontsize=16)\n",
    "    plt.xlabel('Normal quantiles', fontsize=16)\n",
    "    plt.title('QQ plot', fontsize=16)\n",
    "    plt.show()\n",
    "\n",
    "QQ_plot(X, y, b_hat)\n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, the QQ plot shows that the quantiles of the two distributions don't quite align (i.e., the quantiles do not lie near the line $y=x$). As we will see in the next problem, we can sometimes obtain a better fit by transforming the data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2.5 Regression with transformed data\n",
    "\n",
    "One method that is sometimes used to obtain a better fit in linear regression is to transform either the predictor variables or the response variable. Use the functions you've written in problem 2.1-2.5 to fit another regression model, except using $\\log(y)$ as the response rather than $y$. Plot the QQ function of the new regression model. How does it compare the one we obtain in 2.4?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# YOUR CODE GOES HERE\n",
    "# YOUR CODE GOES HERE\n",
    "# YOUR CODE GOES HERE"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "name": "_merged"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
