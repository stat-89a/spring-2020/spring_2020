{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "from linalg_for_datasci.tests import typecheck_series\n",
    "\n",
    "# Uncomment the line below if you want to see all entries of the datasets\n",
    "# pd.set_option('display.max_rows', 251)\n",
    "# Clicking the blue bar to the left of a cell output can be used to hide it"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Examples of generalized inverses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Cities to Cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 States\n",
    "\n",
    "The `states` dataset contains basic information about six states: Alaska, California, Colorado, Hawaii, Washington, and Wyoming."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import states\n",
    "\n",
    "states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above table implicitly assigns to each state its capital. In the code cell below, write this relationship as pairs of keys and values by completing the given Python dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state_capitals_assignment = {'Alaska': ..., 'California': ..., 'Colorado':...,\n",
    "                            'Hawaii': ..., 'Washington': ..., 'Wyoming': ...}\n",
    "state_capitals_assignment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use this relationship between states and their capitals later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Capitals\n",
    "\n",
    "The `cities` dataset includes a set of cities in the same states Alaska, California, Colorado, Hawaii, Washington, and Wyoming. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import western_cities as cities\n",
    "\n",
    "# Which states have cities in the dataset\n",
    "cities[\"State\"].unique()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `cities` dataset also contains information about each of the cities. There are 251 cities in the `cities` dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we will define a function\n",
    "\n",
    "$$ \\text{cities} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "which, given a city, returns the state capital of the state in which the city is located. For example, $\\text{Aiea}$ is in Hawaii, so $f(\\text{Aiea})$ is $\\text{Honolulu}$, since that is the capital of Hawaii.\n",
    "\n",
    "Each row of the pandas dataframe above represents an individual city. The function definition below takes a city as `input_element`, which is represented by the corresponding row of the dataframe reformatted as a pandas series, and then checks the value of `input_element['State']` to see what state the city is located in. It then looks for the city assigned to that state as recorded by the Python dictionary `assignment`. Specifically, the `state` is the key in the Python dictionary, and the city assigned to that state is the corresponding value.\n",
    "\n",
    "(Don't worry if you don't know what pandas dataframes or series are. You don't need to know what they are for this course. You will learn how to use pandas yourself in DATA 100. Pandas is the library which is used to define the `Table` from DATA 8.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def assign_according_to_state(input_element, assignment):\n",
    "    typecheck_series(input_element)\n",
    "    state = input_element[\"State\"]\n",
    "    output_element = assignment[state]\n",
    "    return output_element"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to create a function that assigns to each input city the state capital of the state in which it is located, we need to have a Python dictionary `assignment` which records what the capital of that state is. We already created such a dictionary above! Use the dictionary `state_capitals_assignment` and a lambda function based on `assign_according_to_state` to create a function which assigns each city to the capital of the state in which it's located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "city_to_state_capital = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The code in the following cell shows what the result of applying the function `city_to_state_capital` to every city in the `cities` dataset is. \n",
    "\n",
    "(`axis=1` means that the function is applied to the rows of the dataframe, whereas `axis=0` would denote attempting to apply the function to the columns of the dataframe, which in this case would be impossible.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities.apply(city_to_state_capital, axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A better way to visualize the function $f$ is to have a pandas DataFrame with two columns, with the first column containing the input cities and the second column containing the corresponding output cities."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_capitals = pd.DataFrame()\n",
    "cities_assigned_to_capitals[\"City\"] = cities[\"City\"].values\n",
    "cities_assigned_to_capitals[\"f(City)\"] = cities.apply(city_to_state_capital, axis=1)\n",
    "cities_assigned_to_capitals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that the function $f$ is not one-to-one (injective). For example, the distinct cities of *Corona* and *Palmdale* are assigned to *Sacramento*. Therefore $f$ cannot have any post-inverse (left inverse). \n",
    "\n",
    "Moreover, $f$ is not onto (surjective). For example, *Littleton* is not the capital of any state, so none of the cities are assigned to *Littleton*. Therefore $f$ cannot have any pre-inverse (right inverse).\n",
    "\n",
    "However, in spite of the fact that $f$ has neither a post-inverse nor a pre-inverse, it has several generalized inverses. Below we define a function $g$ and demonstrate that it is an example of a generalized inverse for $f$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.3 Most Populous Cities\n",
    "\n",
    "Here we will define a function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "which assigns to each city the most populous city (as of 2010) of the state in which it is located. For example, $\\text{Palmdale}$ is in California, and $\\text{Los Angeles}$ is the most populous city in California, so $g(\\text{Palmdale}) = \\text{Los Angeles}$. To implement this function, we will again use a lambda function based on `assign_according_to_state` and a Python dictionary which tells us what is the output city for each state.\n",
    "\n",
    "To create such a Python dictionary we first need to know what the most populous city in each state is!\n",
    "\n",
    "The following function takes a pandas DataFrame (e.g. `cities`) where one of the columns is `'State'`, and given a `key` i.e. column name (e.g. `'Population (2010)'`) returns the \"largest\" rows (in terms of the given `key`) for each state. Since you are not expected to know Pandas for this course, it is given to you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def find_largest_per_state(dataframe, key):\n",
    "    return dataframe.loc[dataframe.groupby(\"State\")[key].idxmax()]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the above function to find the most populous city (in 2010) for every state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "most_populous_cities = ...\n",
    "most_populous_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have found the most populous cities in each state, we can now create a Python dictionary where each key is a state and each value is the most populous city in that state. Do so in the code cell below.`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "most_populous_cities_assignment = {'Alaska': ..., 'California': ..., 'Colorado':...,\n",
    "                                   'Hawaii': ..., 'Washington': ..., 'Wyoming': ...}\n",
    "most_populous_cities_assignment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now use the above `most_populous_cities_assignment` and the `assign_according_to_state` function to create a function, `city_to_most_populous_city` which assigns each city to the most populous city of the state in which it is located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "city_to_most_populous_city = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's view the function $g$ as a DataFrame where the first column is the input city and the second column is the output city."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_most_populous_cities = pd.DataFrame()\n",
    "cities_assigned_to_most_populous_cities[\"City\"] = cities[\"City\"].values\n",
    "cities_assigned_to_most_populous_cities[\"g(City)\"] = cities.apply(\n",
    "    city_to_most_populous_city, axis=1\n",
    ")\n",
    "cities_assigned_to_most_populous_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that, similar to $f$, the function $g$ is not one-to-one (injective). For example, the distinct cities of *Corona* and *Palmdale* are assigned to *Los Angeles*. Therefore $g$ cannot have any post-inverse (left inverse). \n",
    "\n",
    "Moreover, similar to $f$, the function $g$ is not onto (surjective). For example, *Littleton* is not the most populous city of any state, so none of the cities are assigned to *Littleton*. Therefore $g$ cannot have any pre-inverse (right inverse).\n",
    "\n",
    "However, again similar to $f$, in spite of the fact that $g$ has neither a post-inverse nor a pre-inverse, it has several generalized inverses, one of which is $f$. We will show this relationship between $f$ and $g$ below."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.4 One Way of Viewing Generalized Inverses\n",
    "\n",
    "Remember that if $f$ and $g$ are generalized inverses of each other, then $f \\circ g \\circ f = f$ and $g \\circ f \\circ g = g$.\n",
    "\n",
    "In the cell below, we compute $f \\circ g \\circ f$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_copy1 = cities.copy()\n",
    "cities_copy1[\"f(City)\"] = cities_copy1.apply(city_to_state_capital, axis=1)\n",
    "cities_copy1[\"(g o f)(City)\"] = cities_copy1.apply(city_to_most_populous_city, axis=1)\n",
    "cities_copy1[\"(f o g o f)(City)\"] = cities_copy1.apply(city_to_state_capital, axis=1)\n",
    "f_g_f = cities_copy1[[\"City\", \"(f o g o f)(City)\"]]\n",
    "f_g_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's make sure that the input cities to both functions are the same and displayed in the same order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_capitals[\"City\"].values == f_g_f[\"City\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also need to check that the value of $f$ and of $f \\circ g \\circ f$ is the same for *every* input city in order to be able to say that they are the same function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_capitals[\"f(City)\"].values == f_g_f[\"(f o g o f)(City)\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparing `cities_asigned_to_capitals` and `f_g_f` side by side should also convince you visually that $f$ and $f \\circ g \\circ f$ are the same function. Is this result surprising to you? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cells below, repeat a similar process to check that $g$ and $g \\circ f \\circ g$ are the same function. Some of the code has already been filled in for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_copy2 = cities.copy()\n",
    "cities_copy2[\"g(City)\"] = ...\n",
    "cities_copy2[\"(f o g)(City)\"] = ...\n",
    "cities_copy2[\"(g o f o g)(City)\"] = ...\n",
    "g_f_g = cities_copy2[[\"City\", \"(g o f o g)(City)\"]]\n",
    "g_f_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we should make sure that the input cities to both functions are the same and displayed in the same order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_most_populous_cities[\"City\"].values == g_f_g[\"City\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, we also need to check that the value of $g$ and of $g \\circ f \\circ g$ is the same for *every* input city in order to be able to say that they are the same function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_most_populous_cities[\"g(City)\"].values == g_f_g[\n",
    "    \"(g o f o g)(City)\"\n",
    "].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparing `cities_asigned_to_most_populous_cities` and `g_f_g` side by side should also convince you visually that $g$ and $g \\circ f \\circ g$ are the same function. Is this result surprising to you? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.5 Another Way of Viewing Generalized Inverses\n",
    "\n",
    "Remember that $f$ and $g$ are generalized inverses of one another if and only if the functions\n",
    "\n",
    "$$ \\operatorname{Im}(g) \\quad \\overset{f_{|\\operatorname{Im}(g)}}{\\longrightarrow} \\quad \\operatorname{Im}(f) $$\n",
    "\n",
    "and\n",
    "\n",
    "$$ \\operatorname{Im}(f) \\quad   \\overset{g_{|\\operatorname{Im}(f)}}{\\longrightarrow} \\quad \\operatorname{Im}(g) $$\n",
    "\n",
    "are inverses of each other. \n",
    "\n",
    "Remember that the image of a function is the subset of all values in the codomain of the function which the function assigns values to, here denoted by $\\operatorname{Im}$. Also remember that given any function, $\\phi: X \\to Y$, $\\operatorname{Im}(\\phi) \\subseteq Y$, and we can define a function which does exactly the same thing but has its codomain restricted only to $\\operatorname{Im}(\\phi)$, which by abuse of notation we denote with the same name $\\phi: X \\to \\operatorname{Im}(\\phi)$. Then remmember that the so-called restriction $\\phi_{|Z}$, where $Z \\subseteq X$, is the function $Z \\to \\operatorname{Im}(\\phi)$ (or $Z \\to Y$ if we restrict the original $\\phi: X \\to Y$, although here we're restricting $\\phi: X \\to \\operatorname{Im}(\\phi)$) which has its domain restricted to $Z \\subseteq X$ and has the same value that $\\phi$ does for every element of $Z$.\n",
    "\n",
    "In order to check whether $f_{|\\operatorname{Im}(g)}$ and $g_{|\\operatorname{Im}(f)}$ are inverses of each other, we first need to know what the subsets $\\operatorname{Im}(g)$ and $\\operatorname{Im}(f)$ of $\\text{cities}$ are.\n",
    "\n",
    "By showing us which cities are assigned as outputs by $f$ without showing repeats (remember that sets don't have repeated elements) the code in the following cell allows us to see what $\\operatorname{Im}(f)$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f = cities_assigned_to_capitals[\"f(City)\"].unique()\n",
    "image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are just the capitals of each of the six states. Is this result surprising to you?\n",
    "\n",
    "Fill in the code in the cell below to see what $\\operatorname{Im}(g)$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_g = ...\n",
    "image_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can check that these are the most populous cities in each of the six states.\n",
    "\n",
    "Note that $\\operatorname{Im}(f)$ and $\\operatorname{Im}(g)$ both have the same number of elements, six, so it is possible for there to exist bijections, i.e. one-to-one and onto functions, i.e. functions with inverses, between the two.\n",
    "\n",
    "The code in the following cell allows us to see what $f_{|\\operatorname{Im}(g)}$ is by filtering out all of the city rows except for those cities in $\\operatorname{Im}(g)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_g = cities_assigned_to_capitals[\"City\"].isin(image_g)\n",
    "f_restricted_to_image_g = cities_assigned_to_capitals.loc[restriction_to_image_g]\n",
    "f_restricted_to_image_g"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the code cell below, modify the code in the cell above to see what $g_{|\\operatorname{Im}(f)}$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f = ...\n",
    "g_restricted_to_image_f = ...\n",
    "g_restricted_to_image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are these two functions inverses of each other? Why or why not? (Remember that the order in which elements of a set are displayed does not matter.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Cities and Mountains\n",
    "\n",
    "The `mountains` dataset includes the set of the 75 tallest mountains in the United States as well as some information about them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import mountains\n",
    "\n",
    "# Which states have mountains in the dataset\n",
    "mountains[\"State\"].unique()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because of the sizes of the datasets ($75$ mountains in `mountains`, $251$ cities in `cities`, $75 < 251$), there exist functions\n",
    "\n",
    "$$ \\text{cities} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{mountains} $$\n",
    "\n",
    "which are post-invertible (one-to-one/left-invertible/injective). But there are no functions from `cities` to `mountains` which are pre-invertible (onto/right-invertible/surjective).\n",
    "\n",
    "For mirror reasons, the sizes of the datasets mean that there exist functions\n",
    "\n",
    "$$ \\text{mountains} \\quad \\overset{g}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "which are pre-invertible (onto/right-invertible/surjective). But there are no functions from `mountains` to `cities` which are post-invertible (one-to-one/left-invertible/injective).\n",
    "\n",
    "You learned previously that any post-inverse is a generalized inverse, and also that any pre-inverse is a generalized inverse. However, in general there also exist many generalized inverses which are neither post-inverses nor pre-inverses, even in the case where no post-inverses or pre-inverses exist. \n",
    "\n",
    "Below we will consider functions which don't have post-inverses or pre-inverses, but which do have generalized inverses."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Tallest Mountains\n",
    "\n",
    "Here we will define a function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{mountains} $$\n",
    "\n",
    "which assigns to each city the tallest mountain of the state in which the city is located.\n",
    "\n",
    "In the cell below, use the `find_largest_per_state` function on the `mountains` dataset to find the tallest mountains in each of the six states."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tallest_mountains = ...\n",
    "tallest_mountains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above table implicitly assigns to each state its tallest mountain. Similar to what you did previously, in the code cell below, write this relationship as pairs of keys in a Python dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tallest_mountains_assignment = {'Alaska': ..., 'California': ..., 'Colorado':...,\n",
    "                                'Hawaii': ..., 'Washington': ..., 'Wyoming': ...}\n",
    "tallest_mountains_assignment"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to what was done above, use the `assign_according_to_state` function and the `tallest_mountains_assignment` dictionary to create a lambda function which assigns each city to the tallest mountain of the state in which it is located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "city_to_tallest_mountain = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's view the function $f$ as a DataFrame where the first column is the input city and the second column is the output mountain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_tallest_mountains = pd.DataFrame()\n",
    "cities_assigned_to_tallest_mountains[\"City\"] = cities[\"City\"].values\n",
    "cities_assigned_to_tallest_mountains[\"f(City)\"] = cities.apply(\n",
    "    city_to_tallest_mountain, axis=1\n",
    ")\n",
    "cities_assigned_to_tallest_mountains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, this function $f$ is neither one-to-one (so it has no post-inverse) nor is it onto (so it has no pre-inverse). *Longmont* and *Thornton* are distinct cities, for example, but they are both assigned to *Mount Elbert*, so $f$ isn't one-to-one, and since *Tower Mountain* isn't the highest mountain in any state, no city is assigned to *Tower Mountain*, so $f$ isn't onto. However, despite not having any post-inverses or pre-inverses, $f$ does have several generalized inverses. In fact, in the problems below, we will define two distinct generalized inverses for $f$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Mountains and State Capitals\n",
    "\n",
    "Here we will define one function\n",
    "\n",
    "$$\\text{mountains} \\quad \\overset{g_1}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "which assigns to each mountain the capital of the state in which it is located. In what follows below we will show that $g_1$ is one generalized inverse for $f$.\n",
    "\n",
    "Using the `assign_according_to_state` function and the `state_capitals_assignment` Python dictionary, create a lambda function `mountain_to_state_capital` representing $g_1$, i.e. which assigns each mountain to the capital of the state in which it is located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountain_to_state_capital = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's view the function $g_1$ as a DataFrame where the first column is the input mountain and the second column is the output city."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_state_capitals = pd.DataFrame()\n",
    "mountains_assigned_state_capitals[\"Mountain\"] = mountains[\"Mountain\"].values\n",
    "mountains_assigned_state_capitals[\"g1(Mountain)\"] = mountains.apply(\n",
    "    mountain_to_state_capital, axis=1\n",
    ")\n",
    "mountains_assigned_state_capitals"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, just like $f$, $g_1$ is neither onto nor one-to-one.\n",
    "\n",
    "One way to show that $f$ and $g_1$ are generalized inverses is by demonstrating that $f \\circ g_1 \\circ f = f$ and $g_1 \\circ f \\circ g_1 = g_1$.\n",
    "\n",
    "By suitably modifying code from problems above, show in the cell below that $f \\circ g_1 \\circ f = f$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_copy3 = cities.copy()\n",
    "cities_copy3[\"f(City)\"] = ...\n",
    "cities_copy3[\"(g1 o f)(City)\"] = ...\n",
    "cities_copy3[\"(f o g1 o f)(City)\"] = ...\n",
    "f_g1_f = cities_copy3[[\"City\", \"(f o g1 o f)(City)\"]]\n",
    "f_g1_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can compare this visually with `cities_assigned_to_tallest_mountains` to see that they are the same. It's also possible to use code to check this the same way as in earlier problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_tallest_mountains[\"City\"].values == f_g1_f[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_tallest_mountains[\"f(City)\"].values == f_g1_f[\n",
    "    \"(f o g1 o f)(City)\"\n",
    "].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By suitably modifying code from problems above, show in the cell below that $g_1 \\circ f \\circ g_1 = g_1$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_copy1 = mountains.copy()\n",
    "mountains_copy1[\"g1(Mountain)\"] = ...\n",
    "mountains_copy1[\"(f o g1)(Mountain)\"] = ...\n",
    "mountains_copy1[\"(g1 o f o g1)(Mountain)\"] = ...\n",
    "g1_f_g1 = mountains_copy1[[\"Mountain\", \"(g1 o f o g1)(Mountain)\"]]\n",
    "g1_f_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can compare this visually with `mountains_assigned_to_state_capitals` to see that they are the same. It's also possible to use code to check this the same way as in earlier problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_state_capitals[\"Mountain\"].values == g1_f_g1[\"Mountain\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_state_capitals[\"g1(Mountain)\"].values == g1_f_g1[\n",
    "    \"(g1 o f o g1)(Mountain)\"\n",
    "].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An equivalent way of verifying that $f$ and $g_1$ are generalized inverses of each other is by showing that $f_{|\\operatorname{Im}(g_1)}$ and ${g_1}_{|\\operatorname{Im}(f)}$ are inverses of each other.\n",
    "\n",
    "Modifying code above, use the following cell to compute what the image of $f$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f = ...\n",
    "image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the same way, use the following cell to compute what the image of $g_1$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_g1 = ...\n",
    "image_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use code similar to that found in the problems of above to see what $f_{|\\operatorname{Im}(g_1)}$ is. Here some of it is already done for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_g1 = cities_assigned_to_tallest_mountains[\"City\"].isin(image_g1)\n",
    "f_restricted_to_image_g1 = ...\n",
    "f_restricted_to_image_g1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, use code to that found in the problems above to see what ${g_1}_{|\\operatorname{Im}(f)}$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f = ...\n",
    "g1_restricted_to_image_f = ...\n",
    "g1_restricted_to_image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are $f_{|\\operatorname{Im}(g_1)}$ and ${g_1}_{|\\operatorname{Im}(f)}$ inverses of each other? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.3 Mountains and Most Populous Cities\n",
    "\n",
    "Here we will define another function\n",
    "\n",
    "$$\\text{mountains} \\quad \\overset{g_2}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "which assigns each mountain to the most populous city in the state in which it is located. In what follows we will show that $g_2$ is another generalized inverse for $f$.\n",
    "\n",
    "Using the `assign_according_to_state` function and the `most_populous_cities_assignment` Python dictionary, create a lambda function `mountain_to_most_populous_city` representing $g_2$, i.e. which assigns each mountain to the most populous city of the state in which it is located."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountain_to_most_populous_city = ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's view the function $g_2$ as a DataFrame where the first column is the input mountain and the second column is the output city."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_most_populous_cities = pd.DataFrame()\n",
    "mountains_assigned_most_populous_cities[\"Mountain\"] = mountains[\"Mountain\"].values\n",
    "mountains_assigned_most_populous_cities[\"g2(Mountain)\"] = mountains.apply(\n",
    "    mountain_to_most_populous_city, axis=1\n",
    ")\n",
    "mountains_assigned_most_populous_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Again, just like $f$, $g_2$ is neither onto nor one-to-one. Nevertheless both functions have generalized inverses.\n",
    "\n",
    "One way to show that $f$ and $g_2$ are generalized inverses is by demonstrating that $f \\circ g_2 \\circ f = f$ and $g_2 \\circ f \\circ g_2 = g_2$.\n",
    "\n",
    "By suitably modifying code from problems above, show in the cell below that $f \\circ g_2 \\circ f = f$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_copy4 = cities.copy()\n",
    "cities_copy4[\"f(City)\"] = ...\n",
    "cities_copy4[\"(g2 o f)(City)\"] = ...\n",
    "cities_copy4[\"(f o g2 o f)(City)\"] = ...\n",
    "f_g2_f = cities_copy4[[\"City\", \"(f o g2 o f)(City)\"]]\n",
    "f_g2_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can compare this visually with `cities_assigned_to_tallest_mountains` to see that they are the same. It's also possible to use code to check this the same way as in earlier problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_tallest_mountains[\"City\"].values == f_g2_f[\"City\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cities_assigned_to_tallest_mountains[\"f(City)\"].values == f_g2_f[\n",
    "    \"(f o g2 o f)(City)\"\n",
    "].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By suitably modifying code from problems above, show in the cell below that $g_2 \\circ f \\circ g_2 = g_2$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_copy2 = mountains.copy()\n",
    "mountains_copy2[\"g2(Mountain)\"] = ...\n",
    "mountains_copy2[\"(f o g2)(Mountain)\"] = ...\n",
    "mountains_copy2[\"(g2 o f o g2)(Mountain)\"] = ...\n",
    "g2_f_g2 = mountains_copy2[[\"Mountain\", \"(g2 o f o g2)(Mountain)\"]]\n",
    "g2_f_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can compare this visually with `mountains_assigned_to_most_populous_cities` to see that they are the same. It's also possible to use code to check this the same way as in earlier problems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_most_populous_cities[\"Mountain\"].values == g2_f_g2[\"Mountain\"].values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mountains_assigned_most_populous_cities[\"g2(Mountain)\"].values == g2_f_g2[\n",
    "    \"(g2 o f o g2)(Mountain)\"\n",
    "].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An equivalent way of verifying that $f$ and $g_2$ are generalized inverses of each other is by showing that $f_{|\\operatorname{Im}(g_2)}$ and ${g_2}_{|\\operatorname{Im}(f)}$ are inverses of each other.\n",
    "\n",
    "Modifying code above, use the following cell to compute what the image of $f$ is. (Hint: you did this once already in the problem above.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_f = ...\n",
    "image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the same way, use the following cell to compute what the image of $g_2$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_g2 = ...\n",
    "image_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use code similar to that found in the problems of above to see what $f_{|\\operatorname{Im}(g_2)}$ is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_g2 = ...\n",
    "f_restricted_to_image_g2 = ...\n",
    "f_restricted_to_image_g2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly, use code to that found in the problems above to see what ${g_2}_{|\\operatorname{Im}(f)}$ is. Here some of the code is already filled in for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "restriction_to_image_f = mountains_assigned_most_populous_cities[\"Mountain\"].isin(\n",
    "    image_f\n",
    ")\n",
    "g2_restricted_to_image_f = ...\n",
    "g2_restricted_to_image_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Are $f_{|\\operatorname{Im}(g_2)}$ and ${g_2}_{|\\operatorname{Im}(f)}$ inverses of each other? Why or why not?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
