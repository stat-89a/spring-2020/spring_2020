{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Functions with pre-inverses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1 Western Cities\n",
    "\n",
    "The `western_cities` dataset includes a set of cities in the Western states Alaska, California, Colorado, Hawaii, Washington, and Wyoming. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import western_cities\n",
    "\n",
    "western_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `western_cities` dataset also contains information about each of the cities. There are 251 cities in the `western_cities` dataset.\n",
    "\n",
    "Observe how the above table implicitly assigns each city to the state in which it is located. In other words, if we ignore the population column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "western_cities = western_cities[[\"City\", \"State\"]]\n",
    "western_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "then the above table implicitly defines a function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g}{\\longrightarrow} \\quad \\text{states} $$\n",
    "\n",
    "given by assigning each city to the state in which it is located.\n",
    "\n",
    "The function $g$ is onto, i.e. every state in the `states` dataset is the location of at least one city in the `western_cities` dataset. To see this, we can look at the image of $g$ by computing all of the unique values in the `'State'` column of `western_cities`. (In other words, by showing each state that appears only once, rather than every time it appears.) The code in the following cell does this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "western_cities[\"State\"].unique()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible for $g$ to be an onto function because the number of elements in its domain, `western_cities`, is $251$, and the number of elements in its codomain, `states`, is $6$, and $251 > 6$. Because $g$ is onto, it has at least one pre-inverse. In fact, because $g$ is onto but not one-to-one, it's guaranteed to have more than one pre-inverse. (Exercise: try to explain why a function which is onto but not one-to-one must have more than one pre-inverse.) To see that $g$ is not one-to-one, note for example how both *Corona* and *Pomona* are assigned to *California* even though they are distinct cities, or in other words observe that $g(\\text{Corona})=\\text{California}=g(\\text{Pomona})$ even though $\\text{Corona}\\not=\\text{Pomona}$. (Hint: given a pre-inverse of $g$ which assigns *California* to *Pomona*, we can always get a new and distinct inverse by changing the original pre-inverse to now assign *California* to *Pomona*. So $g$ must have more than one pre-inverse if it has any because it is not one-to-one.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function $g$ effectively classifies each city according to which state it belongs to. Thus any pre-inverse of $g$ will be a function which assigns each state a unique representative of the cities that belong to it. Below we demonstrate at least two distinct ways in which this can be done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 State Capitals\n",
    "\n",
    "The `states` dataset contains basic information about six states: Alaska, California, Colorado, Hawaii, Washington, and Wyoming."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import states\n",
    "\n",
    "states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above table implicitly assigns to each state its capital. In other words, if we ignore the population column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states = states[[\"State\", \"Capital\"]]\n",
    "states"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "then the above table implicitly defines a function\n",
    "\n",
    "$$\\text{states} \\quad \\overset{f_1}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "given by assigning to each state its capital. \n",
    "\n",
    "The claim is that $f_1$ is a pre-inverse to the function $g$ defined above, which assigns to each city the state in which it is located. Intuitively, this is obvious since of course the capital of each state is located in the state of which it is the capital. Here we will also demonstrate this using code. \n",
    "\n",
    "Note also that it is intuitively obvious that $f_1$ should be one-to-one (and thus possibly the pre-inverse of some other function): of course there is no city which is the capital of more than one state. This is possible since the number of elements in the set $\\text{states}$ (the domain of $f_1$) is $6$ while the number of elements in the set $\\text{cities}$ (the codomain of $f_1$) is $251$, and clearly $6 < 251$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to show that $f_1$ is a pre-inverse for $g$, it suffices to show that $g \\circ f_1$ is the identity function on $\\text{states}$. In order to compute the composition of these functions, we can do a join of the tables that represent them. \n",
    "\n",
    "If both of the functions $\\phi_1$ and $\\phi_2$ are represented as tables in the way above (tables have two column, first column is domain of function, all inputs, second column is the value of the function applied to each input, i.e. all outputs, so that the unique values are the image of the function), then the composition $\\phi_2 \\circ \\phi_1$ is well-defined when a left-join of the second column corresponding to $\\phi_1$ with the first column corresponding to $\\phi_2$ is possible. (Such a join is possible if and only if the image of $\\phi_1$, the unique values of the second column of $\\phi_1$, is a subset of the domain of $\\phi_2$, the first column of $\\phi_2$. Then the codomain of $\\phi_1$ can be chosen to be the domain of $\\phi_2$, and $\\phi_2 \\circ \\phi_1$ can be made to be well-defined.)\n",
    "\n",
    "Here we will use the Pythons library `pandas` to represent and perform operations on tables. The `pandas` library is what is working \"under the hood\" or \"beneath the covers\" of the `Table` tables from DATA 8. Don't worry if you don't know how to use `pandas`, since you don't need to know how to for this course. We will give you all code necessary to complete the assignment, and using `pandas` in this course is only meant as a preview for DATA 100, where you'll learn how to use `pandas`.\n",
    "\n",
    "The cell below performs a left join of the table corresponding to $f_1$, namely `states`, with the table corresponding to $g$, namely `western_cities`, along the second column of `states` and the first column of `western_cities`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_copy1 = states.copy()\n",
    "states_copy1.columns = [\"State\", \"City\"]\n",
    "western_cities_copy1 = western_cities.copy()\n",
    "western_cities_copy1.columns = [\"City\", \"(g o f1)(State)\"]\n",
    "g_f1 = states_copy1.merge(western_cities_copy1, how=\"left\", on=\"City\").drop(\n",
    "    \"City\", axis=1\n",
    ")\n",
    "g_f1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see clearly from the table above, $g \\circ f_1$ is the identity function on `states`, so therefore $f_1$ is a pre-inverse for $g$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2 Most Populous Cities\n",
    "\n",
    "We can play with the data from the `western_cities` dataset to find out what the most populous city in each of the six given states is."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import western_cities as western_cities_copy2\n",
    "\n",
    "western_cities_copy2 = western_cities_copy2.loc[\n",
    "    western_cities_copy2.groupby(\"State\")[\"Population (2010)\"].idxmax\n",
    "]\n",
    "most_populous_cities = western_cities_copy2[[\"State\", \"City\"]].reset_index(drop=True)\n",
    "most_populous_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above table implicitly defines a function\n",
    "\n",
    "$$\\text{states} \\quad \\overset{f_2}{\\longrightarrow} \\quad \\text{cities}$$\n",
    "\n",
    "which assigns each state the most populous city (as of 2010) located in that state. Just like $f_1$, $f_2$ is one-to-one and a function from a set with a smaller number of elements ($6$) to a set with a larger number of elements ($251$). The function $f_2$ is also a pre-inverse for $g$, as we will show in the cell below.\n",
    "\n",
    "Most of the code in the cell below is already written for you. For the remaining line of code, modify the code from the previous problem to define `g_f2`, the table representing the function ${g \\circ f_2: \\text{states} \\longrightarrow \\text{states}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states_copy2 = most_populous_cities.copy()\n",
    "states_copy2.columns = [\"State\", \"City\"]\n",
    "western_cities_copy3 = western_cities.copy()\n",
    "western_cities_copy3.columns = [\"City\", \"(g o f2)(State)\"]\n",
    "g_f2 = ...\n",
    "g_f2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see clearly from the table above, $g \\circ f_2$ is also the identity on $\\text{states}$, therefore $f_2$ is another pre-inverse for $g$.\n",
    "\n",
    "Although its fairly visually obvious comparing `states` (corresponding to $f_1$) and `most_populous_cities` (corresponding to $f_2$) that the $f_1$ and $f_2$ are distinct functions, we can also double-check this using code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "states[\"Capital\"].values == most_populous_cities[\"City\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see above, $f_1$ and $f_2$ don't assign the same value to every state. While the capitals and most populous cities of Alaska (Juneau respectively Anchorage), California (Sacramento respectively Los Angelese), and Washington (Olympia respectively Seattle) are all different (so that $f_1 \\not= f_2$), it is nevertheless also true that the capital and the most populous city _are_ the same for Colorado (Denver), Hawaii (Honolulu), and Wyoming (Cheyenne). So even though $f_1$ and $f_2$ are the same for _some_ states, the fact that they are different for _any_ states means that they are not equal, i.e. that they are distinct functions, and thus that $g$ has at least two different pre-inverses, corresponding to the fact that $g$ is onto but not one-to-one."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Functions with post-inverses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Metropolitan Areas and Eastern Cities\n",
    "\n",
    "The `metropolises` dataset contains information about $6$ metropolitan areas, or more specifically Metropolitan Statistical Areas, or MSAs, as defined by the United States Office of Management and Budget (OMB), all located in the Eastern part of the United Sates. (If you're willing to consider the Midwest and South as also being part of the \"East\".)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import metropolises\n",
    "\n",
    "metropolises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If one ignores the population column:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metropolises = metropolises[[\"Metropolitan Statistical Area\", \"Principal City\"]]\n",
    "metropolises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "then the above table implicitly defines a function\n",
    "\n",
    "$$\\text{metropolitan areas} \\quad \\overset{f}{\\longrightarrow} \\quad \\text{cities} $$\n",
    "\n",
    "given by assigning to each metropolitan area its principal (i.e. most important, largest, central) city.\n",
    "\n",
    "The set of cities under consideration here can be found in the dataset `eastern_cities`, describing $9$ cities from the Eastern part of the United States."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import eastern_cities\n",
    "\n",
    "eastern_cities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function $f$ is one-to-one, since no city is the principal city of more than one metropolitan area. This is possible since the number of elements in the domain, `metropolises`, of $f$ is $6$, while the number of elements in the codomain, `eastern_cities`, of $f$ is $9$, and of course $6 < 9$. \n",
    "\n",
    "Note also that $f$ is not onto, since for example Newark is not the principal city of any metropolitan area. (Although Newark is in the New York-Newark-Jersey City metropolitan area, just like Jersey City it is much smaller than New York City, and the economic activity of Newark and Jersey City is centered on what happens in New York City but not vice versa. So New York City is the only principal city of the New York-Newark-Jersey City metropolitan area.) In fact, since the number of elements in the domain of $f$ ($6$) is strictly smaller than the number of elements in the codomain of $f$ ($9$), it is _impossible_ for any function from `metropolises` to `eastern_cities` to be onto.\n",
    "\n",
    "Since $f$ is one-to-one, it has at least one post-inverse. In fact, since $f$ is one-to-one but not onto, it must have more than one post-inverse. (Exercise: come up with an argument to explain why a function which is one-to-one but not onto _must_ have more than one post-inverse.)\n",
    "\n",
    "The function $f$ effectively chooses a representative city for each metropolitan area. Therefore, any post-inverse for $f$ will \"classify\" cities by assigning distinct groups of them to distinct metropolitan areas, and in such a way that the principal city of each metropolitan area is assigned to its metropolitan area. Below we show two distinct ways in which this can be done."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.1 Courts of Appeals\n",
    "\n",
    "In the United States, there are $13$ appellate courts that sit below the Supreme Court. These appellate courts are called _Courts of Appeals_. (The \"appellate\" in \"appellate court\" is related to the word \"appeal\", as in \"to appeal a (legal) case\" or \"Courts of Appeals\".) There are $94$ federal court districts, which are organized into $12$ so-called \"circuits\" according to region. Each \"circuit\" or region has its own Court of Appeals, thus these are sometimes also called _Circuit Courts_. Each Circuit Court is headquartered in a courthouse that is usually located in a major metropolitan area.\n",
    "\n",
    "The `circuit_courts` dataset describes the Circuit Court where each of the $9$ cities from `eastern_cities` is located. More specifically, the dataset: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import circuit_courts\n",
    "\n",
    "circuit_courts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "implicitly defines a function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g_1}{\\longrightarrow} \\quad \\text{metropolitan areas} $$\n",
    "\n",
    "that assigns to each city the metropolitan area where the courthouse of its Circuit Court is located. For example, the courthouse of the Ninth Circuit is located in San Francisco, so any city in the Ninth Circuit (comprising the states of Washington, Oregon, Idaho, Montana, Nevada, Arizona, and California) would be assigned to the San Francisco-Oakland-Berkeley Metropolitan Statistical Area. No state is divided up between multiple circuits, and most circuits correspond to multiple states.\n",
    "\n",
    "The function $g_1$ as defined above is onto, since all $6$ of the metropolitan areas in `metropolises` contains a courthouse of a U.S. Circuit Court. Modifying code from a previous problem, demonstrate in the cell below that $g_1$ is onto. (Hint: $g_1$ is onto if and only if the image of $g_1$ is the same as `metropolises`. If the table `circuit_courts` represents $g_1$, then how can we use code to compute what the image of $g_1$ is?)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_g1 = ...\n",
    "set(image_g1) == set(metropolises[\"Metropolitan Statistical Area\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible for $g_1$ to be onto since the number of elements in its domain ($9$) is larger than the number of elements in its codomain ($6$).\n",
    "\n",
    "However, although $g_1$ is onto, it is not one-to-one. For example, both the cities Des Moines and St. Louis correspond to circuit courts whose courthouse is located in the St. Louis metropolitan area, i.e. $g_1(\\text{Des Moines}) = g_1(\\text{St. Louis})$, but of course Des Moines and St. Louis are two distinct cities ($\\text{Des Moines} \\not= \\text{St. Louis}$). The fact that the number of elements in its domain is strictly larger than the number of elements in its codomain means that it would be impossible for _any_ function ${\\text{cities} \\longrightarrow \\text{metropolitan areas}}$ to be one-to-one.\n",
    "\n",
    "Notice also that $g_1$ is distinct from the function ${\\text{cities} \\longrightarrow \\text{metropolitan areas}}$ which assigns each city to the metropolitan area in which the city is located. Not only is it the case that Burlington and Des Moines are located in metropolitan areas not included in the `metropolises` dataset, but (since Newark is in the state of New Jersey, which corresponds to the 3rd Circuit) Newark is assigned to the Philadelphia-Camden-Wilmington metropolitan area even though Newark is located in the New York-Newark-Jersey City metropolitan area!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The claim is that $g_1$ defined above is one possible post-inverse for $f$. In the cell below, modify code from previous problems to show that $g_1 \\circ f$ is the identity function on metropolitan areas. Some of the code is already given for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metropolises_copy1 = metropolises.copy()\n",
    "metropolises_copy1.columns = [\"Metropolitan Area\", \"City\"]\n",
    "eastern_cities_copy1 = circuit_courts.copy()\n",
    "eastern_cities_copy1.columns = [\"City\", \"(g1 o f)(Metropolitan Area)\"]\n",
    "g1_f = ...\n",
    "g1_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As can be seen clearly from the table above, $g_1 \\circ f$ is the identity function on `metropolises`, so $g_1$ is a post-inverse for $f$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2.2 Federal Reserve Districts\n",
    "\n",
    "The central banking system of the United States, the Federal Reserve System, is organized into twelve regional banks, called (unsurprisingly) Federal Reserve Banks. The regions are called _Federal Reserve Districts_, and were defined in the Federal Reserve Act of 1913 which created the Federal Reserve System. Unlike the Circuits of the federal court system, several states are split between multiple Federal Reserve Districts. In particular, the regions of the United States defined by the federal court system are different (albeit also similar) to the regions of the United States defined by the Federal Reserve System.\n",
    "\n",
    "The `federal_reserve_districts` dataset describes the Federal Reserve District in which each of the $9$ cities from `eastern_cities` is located. More specifically, the dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from linalg_for_datasci.datasets import federal_reserve_districts\n",
    "\n",
    "federal_reserve_districts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "implicitly defines a function\n",
    "\n",
    "$$\\text{cities} \\quad \\overset{g_2}{\\longrightarrow} \\quad \\text{metropolitan areas} $$\n",
    "\n",
    "which assigns to each city the metropolitan area in which the city's corresponding Federal Reserve Bank is located. For example, Salt Lake City in Utah is in the 12th Federal Reserve District (but not the 9th Circuit) and so it would be assigned to the San Francisco-Oakland-Berkeley metropolitan area, where the headquarters of the bank of the 12th Federal Reserve District is located.\n",
    "\n",
    "Just like $g_1$, $g_2$ is also onto but not one-to-one. (For example, Des Moines and Chicago are distinct cities, i.e. $\\text{Des Moines} \\not= \\text{Chicago}$, but nevertheless both are located in the same Federal Reserve District, i.e. $g_2(\\text{Des Moines}) = g_2(\\text{Chicago})$.) Again, this matches the fact that there are strictly more elements in the domain of $g_2$ than in the codomain of $g_2$.\n",
    "\n",
    "Demonstrate in the cell below that $g_2$ is onto by modifying code from a previous problem. Some of it is already filled in for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image_g2 = ...\n",
    "set(image_g2) == set(metropolises[\"Metropolitan Statistical Area\"].values)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The claim is that $g_2$ is also a post-inverse for $f$, or equivalently that $g_2 \\circ f$ is the identity function on metropolitan areas. In the cell below, modify code from previous problems to show that this is true. Note that again some of the code is already filled in for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metropolises_copy2 = metropolises.copy()\n",
    "metropolises_copy2.columns = [\"Metropolitan Area\", \"City\"]\n",
    "eastern_cities_copy2 = federal_reserve_districts.copy()\n",
    "eastern_cities_copy2.columns = [\"City\", \"(g2 o f)(Metropolitan Area)\"]\n",
    "g2_f = ...\n",
    "g2_f"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This shows clearly that $g_2 \\circ f$ is the identity on metropolitan areas, so therefore $g_2$ truly is a post-inverse for $f$.\n",
    "\n",
    "It can be seen visually that $g_2$ is a distinct post-inverse from $g_1$ by comparing `circuit_courts` (corresponding to $g_1$) and `federal_reserve_districts` (corresponding to $g_2$), but we can also double-check this using code.\n",
    "\n",
    "Modifying code from a previous problem, show that $g_1$ and $g_2$ are distinct functions, and thus that $f$ has at least two distinct post-inverses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit_courts[\"Circuit Court Location\"].values == ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "While the location of the Circuit Court and Federal Reserve Bank are the same for Philadelphia, New York City, Boston, St. Louis, Chicago, and Atlanta, the above cell shows that they are _not_ the same for Burlington, Newark, and Des Moines. So although $g_1$ and $g_2$ agree for some values, they do not agree for all values, and therefore are distinct functions.\n",
    "\n",
    "Also, as a sanity check, we can verify using code that the result above saying that $g_1$ and $g_2$ are distinct is _not_ due to the cities being displayed in different orders in `circuit_courts` and `federal_reserve_districts`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit_courts[\"City\"].values == federal_reserve_districts[\"City\"].values"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Challenge:** There is a specific subset of cities for which _any_ two post-inverses of $f$ _must_ agree. (Otherwise at least one of them would not be a post-inverse for $f$.) What is this subset? Why?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
